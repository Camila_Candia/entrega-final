<!--Ventana de la lista de los datos de la tabla-->
<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Lista de datos de Asignar Equipos</title>
    </head>

    <body>
        <!--Bloque php en donde se hace el llamado del archivo donde se realiza el proceso de eliminar datos-->
        <?php
            //hace el llamado de la conexión de la base de datos
            include "Conexion_BD.php";

            //se pregunta si hay datos relacinados con la id y entra en el ciclo
            if (isset($_GET['id']))
            {

                //hace el llamdo donde se hace el proceso de eliminar los datos 
                include "Eliminar_DAE.php";

            }

        ?>

        <table>

            <tr>
                <!--Titulos de la lista-->
                <th>Fecha</th>
                <th>Recurso</th>
                <th>N° de Sereial</th>
                <th>N° de Tarjeta</th>
                <th>U/A</th>
            </tr>

            <tbody>

                <!--Bloque php en donde se hace el llamdo a la conexion de la base datos y al archivo donde se procesa los datos para mostralos en pantalla-->
                <?php

                    //hace llamado a la conexion de la base datos
                    include "Conexion_BD.php";
                    include "Selecionar_DAE.php";

                ?>
        
            </tbody>   
        
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="Asignar_Equipos.php">
            <input type="submit" value="Volver al formulario" />
        </form> 

        <a href="Reporte_word.php" >Reporte en Word</a>
            <a href="Reporte_excel.php" >Reporte en Excel</a>


    </body>

</html>
