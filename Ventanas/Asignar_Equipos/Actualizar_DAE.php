<?php
    //Actualizar los datos 
    //pregunta si los campos estan vacios si no lo estan entran al ciclo 
    if (isset($_POST['Fecha']) and isset($_POST['Recurso']))
    {

        //se capturan todos los datos que inserta el usuario 
        $id=$_POST['id']; //la id se incrementa sola
        $fecha=$_POST['Fecha'];
        $recurso=$_POST['Recurso'];

        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se hace la consulta sql para la tabla de asignar equipos
        $consulta_1=$conexion->prepare("UPDATE asignar_equipos SET fecha=:fecha, recurso=:recurso WHERE id_ae=:id");

        //se introducen los nuevos datos
        $consulta_1->bindParam(":fecha", $fecha);
        $consulta_1->bindParam(":recurso",$recurso);
        $consulta_1->bindParam(":id",$id);
        
        //ejecutamos la consulta
        $consulta_1->execute();
        
        //se captura los datos que inserta el usuario
        $n_serial=$_POST['N_de_Serial'];
        $n_tarejeta=$_POST['N_de_Tarjeta'];
        $u_a=$_POST['U/A'];

        //se hace la conexion de la base datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
        //se hace la consulta sql para la tabla de asignar equipos detalle
        $consulta_2=$conexion->prepare("UPDATE asignar_equipos_det SET numero_de_serial=:n_serial, numero_de_tarjeta=:n_tarjeta, u_a=:u_a WHERE id_ae=:id");
        
        //se introducen los nuevos datos
        $consulta_2->bindParam(":n_serial",$n_serial);
        $consulta_2->bindParam(":n_tarjeta",$n_tarejeta);
        $consulta_2->bindParam(":u_a", $u_a);
        $consulta_2->bindParam(":id",$id);
        
        //ejecutamos la consulta
        $consulta_2->execute();

    }

    //Recuperar los datos
    //se pregunta si la id no esta vacia, si no lo esta entra en el ciclo y realiza la recupueración de los datos y los muestra en pantalla
    if(isset($_GET['id']))
    {

        $id=$_GET['id'];
        
        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se seleccionan todos los datos de la tabla asignar equipos
        $consulta_1=$conexion->prepare("SELECT * FROM asignar_equipos WHERE id_ae=:id");
        
        //se recuperan todos los datos de la tabla asignar equipos
        $consulta_1->bindParam(":id",$id);
        
        //Eejecutamos la consulta
        $consulta_1->execute();

        //se seleccionan todos los datos de la tabla asignar euipos detalles
        $consulta_2=$conexion->prepare("SELECT * FROM asignar_equipos_det WHERE id_ae=:id2");

        //se recuperan todas los datos de la tabla asignar equipos detalles
        $consulta_2->bindParam(":id2",$id);

        //ejecutamos la consulta
        $consulta_2->execute();

        //se pregunta si las tablas estan vacias si no la estan ingresan al ciclo
        if($consulta_1->rowCount()>=1 and $consulta_2->rowCount()>=1);
        { 

            //se ponen en un array asociativo para poder recorrerlas 
            $ae=$consulta_1->fetch();//en ae estan los datos de la tabla asignar equipos 
            $ae_det=$consulta_2->fetch();//en ae_det estan los datos de la tabla asignar equipos

            //se muestra el formulario en pantalla para actualizacion 
            echo    '<form action="" method="POST">
                        <!--Se trae el id de la tabla asignar equipos pero no se muestra en pantalla-->
                        <input type="hidden" name="id" value="'.$ae['id_ae'].'">

                        <table>

                            <tr>    
                                <td>Fecha:</td>
                                <td><input type="date" name="Fecha" value="'.$ae['fecha'].'" placeholder="dd/mm/aa" required autofocus></td>
                            </tr>  
                            
                            <tr>
                                <td>Recurso:</td>
                                <td><input type="number" name="Recurso" value="'.$ae['recurso'].'" placeholder="Número de Recurso" required></td>
                            </tr>
                            
                            <input type="hidden" name="id" value="'.$ae_det['id_ae'].'">
                            <tr>
                                <th>N° de Serial</th>
                                <th>N° de Trajeta</th>
                                <th>U/A</th>
                            </tr>
                            
                            <table>

                                <tr>
                                    <td><input type="text" name="N_de_Serial" value="'.$ae_det['numero_de_serial'].'" placeholder="N° de Serial"></td>
                                    <td><input type="text" name="N_de_Tarjeta" value="'.$ae_det['numero_de_tarjeta'].'" placeholder="N° de Tarjeta"></td> 
                                    <td><input type="text" name="U/A" value="'.$ae_det['u_a'].'" placeholder="U/A"></td>        
                                </tr>

                            </table>
                            
                            <tr>
                                <td><input type="submit" value="Guardar"></td>
                                <td><input type="reset" value="Borrar"></td>
                            </tr>
                            
                        </table>

                    </form>';

        } 
        
    }
    else
    {
        echo "Error no se pudo procesar la solicitud";
    }
    
?>
