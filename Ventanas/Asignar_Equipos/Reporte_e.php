<?php
   header("Content-type: application/vnd.ms-excel");
   header("Content-Disposition: attachment; filename=Reporte.xls");
   header("Pragma: no-cache");
   header("Expires: 0");


   //conexion a la base de datos 
   $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
  
   //sentencia donde se solicita los datos que existen en la tabla asignar equipos
   $consulta_1= $conexion->prepare( "SELECT id_ae, fecha, recurso FROM asignar_equipos");
   
   //ejecutamos la sentencia
   $consulta_1->execute();

   //sentencia donde se solicita los datso que existen la tabla asiganar_equipos_det
   $consulta_2= $conexion->prepare("SELECT numero_de_serial, numero_de_tarjeta, u_a FROM asignar_equipos_det");

   //ejecutamos la sentencia
   $consulta_2->execute();

   //se cuenta cuantas filas exiten en cada tabla si es mayor a 1 entra en el 
   if($consulta_1->rowCount()>=1 and $consulta_2->rowCount()>=1)
   {

      //se crea un array asociativo de las dos tablas para recorrerlas 
      while($ae=$consulta_1->fetch() and $ae_det=$consulta_2->fetch())
      {

         //se imprime en pantalla lo que existen en las tablas 
         echo "<tr> 
                  <td>".$ae['fecha']."</td>
                  <td>".$ae['recurso']."</td>
                  <td>".$ae_det['numero_de_serial']."</td>
                  <td>".$ae_det['numero_de_tarjeta']."</td>
                  <td>".$ae_det['u_a']."</td>
               </tr>";
         
      }
      
   } 
   else 
   {

      //si no hay ningun dato imprime lo siguiente
      echo "No existe ningun dato";

   }

?>
