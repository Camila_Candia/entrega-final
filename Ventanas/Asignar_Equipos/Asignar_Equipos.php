<!--Ventana de Asignar Equipos-->
<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <!--El título que aparece en la pestaña-->
        <title>Asignar Equipos</title>
        <!--Estilos que se le aplican a la ventana--> 
        <link rel="stylesheet" href="Estilos/estilos.css">
    </head>

    <body>

    <header>
		<nav class="navegacion">
			<ul class="menu">
				<li><a href="#">Administración</a>
					<ul class="submenu">
						<li><a href="../CMR/CMR.php">Consumo de Materiales por Recurso</a></li>
						<li><a href="../CBR/CBR.php">Control de Bobinas por Recurso</a></li>
						<li><a href="../Inventario/Inventario.php">Inventario Semanal Interno</a></li>
					</ul>
				</li>
				<li><a href="#">Depósito</a>
					<ul class="submenu">
						<li><a href="../Asignar_Equipos/Asignar_Equipos.php">Asignación de Equipos</a></li>
						<li><a href="../Movimientos/Movimientos.php">Movimientos</a></li>
						<li><a href="../SMR/SMR.php">Salida de Materiales por Recurso</a></li>
					</ul>
				</li>
				<li><a href="../Equipos_Retirados/Equipos_Retirados.php">Mantenimiento</a></li>
				<li><a href="#">Reportes</a></li>
			</ul>
        </nav>
	</header>
        <!--Título principal-->
        <h1>Asiganar Equipos</h1>


        <div id="main">

            <!--Formulario donde el usuario caraga los datos de los materiales que los tenicos utilizan-->
            <form action="Datos_AE.php" method="POST">

                <table>

                    <tr>    
                        <td>Fecha:</td>
                        <td><input type="date" name="Fecha" placeholder="dd/mm/aa" required autofocus></td>
                    </tr>  
                    
                    <tr>
                        <td>Recurso:</td>
                        <td>
                            <select name="Recurso" id="Deposito">
                            
                                <?php

                                    //incluimos el archivo donde se hace el proceso de mostras los depositos exstentes
                                    include "../../Depositos.php";

                                ?>
                            </select>
                            <!--<input list="Recursos" name="Recurso" placeholder="Número de Recurso" required>-->
                        </td>
                    </tr>

                    <tr>
                        <th>N° de Serial</th>
                        <th>N° de Tarjeta</th>
                        <th>U/A</th>
                    </tr>
                    
                    <table>

                        <tr>
                            <td><input type="text" name="N_de_Serial" placeholder="N° de Serial"></td>
                            <td><input type="text" name="N_de_Tarjeta" placeholder="N° De Tarjeta"></td> 
                            <td><input type="text" name="U/A" placeholder="U/A"></td>
                        </tr>

                    </table>
                    
                    <tr>
                        <td><input type="submit" value="Guardar"></td>
                        <td><input type="reset" value="Borrar"></td>
                    </tr>
                    
                </table>

            </form>

            <!--Botones que hay que revisar si es que se puede mejorar-->
            
            <!--Boton salir, envia a la pagina del menu-->
            <form action="../../index.php">
                <input type="submit" value="Salir" />
            </form>    
            
            <!--Boton que muestra lo que se cargo en la tabla--> 
            <form action="Listado.php">
                <input type="submit" value="Lista de Datos"/>
            </form>

        </div>

    </body>

</html>
