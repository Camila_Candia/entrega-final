<?php
    //PROCESO DE INSERTAR DATOS EN LA BASE DE DATOS 

    //conexion a la base de datos 
    $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
    //captura de datos que el usuario introduce en el formulario 
    $fecha= $_POST['Fecha'];
    $recurso= $_POST['Recurso'];
    $hoy = date("Y:m:d:h:i:sa");
    
    //sentencia sql que prepara los datos en la tabla de asignar equipos
    $consulta_1= "INSERT INTO asignar_equipos (fecha,recurso,fecha_creacion) VALUES (?,?,?) RETURNING id_ae";
    $sql_1=$conexion->prepare($consulta_1);

    //se insertan los datos en la tabla de asignar equipos
    $sql_1->bindParam(1,$fecha);
    $sql_1->bindParam(2,$recurso);
    $sql_1->bindParam(3,$hoy);

    //ejecutamos la sentencia
    $sql_1->execute();

    //los datos que se insertaron en la tabla se cola en un array asociativo
    $id=$sql_1->fetch(PDO::FETCH_ASSOC);
    
    //se pone dentro de una variable la id de la tabla de asignar equipos que tambien esta presente en la tabla de asignar equipos detalle
    $id_ae=$id["id_ae"];

    //echo $id_ae;
    
    //Si existe algun erro impreme lo siguiente
    //echo "Ocurrio un error en la tabla de Asigna Equipos" ;

    //captura de datos que el usuario introduce en el formulario 
    $n_serial=$_POST['N_de_Serial'];
    $n_tarjeta=$_POST['N_de_Tarjeta'];
    $u_a=$_POST['U/A'];

    //sentencia sql que prepara los datos en la tabla de asignar equipos detalle
    $consulta_2= "INSERT INTO asignar_equipos_det (id_ae,numero_de_serial,numero_de_tarjeta,u_a) VALUES (?,?,?,?)";
    $sql_2=$conexion->prepare($consulta_2);
        
    //se insertan los datos en la tabla de asignae equipos detalles
    $sql_2->bindParam(1,$id_ae);
    $sql_2->bindParam(2,$n_serial);
    $sql_2->bindParam(3,$n_tarjeta);
    $sql_2->bindParam(4,$u_a);
        
    //ejecutamos la sentencia
    $sql_2->execute();
                
    //Si ocurre un error imprime lo siguiente
    //echo 'Ocurrio un error en la tabla de Asignar Equipos Detalle';

    //recarga la pagina despues de darle el boton guaradar 
    header("Location: Asignar_Equipos.php");

?>
