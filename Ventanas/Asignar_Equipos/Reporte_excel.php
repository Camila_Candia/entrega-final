<!--Ventana de la lista de los datos de la tabla-->
<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Lista de datos de Asignar Equipos</title>
    </head>

        <table>

            <tr>
                <!--Titulos de la lista-->
                <th>Fecha</th>
                <th>Recurso</th>
                <th>N° de Sereial</th>
                <th>N° de Tarjeta</th>
                <th>U/A</th>
            </tr>

            <tbody>

                <!--Bloque php en donde se hace el llamdo a la conexion de la base datos y al archivo donde se procesa los datos para mostralos en pantalla-->
                <?php

                    //hace llamado a la conexion de la base datos
                    include "Conexion_BD.php";
                    include "Reporte_w.php";

                ?>
        
            </tbody>   
        
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="Asignar_Equipos.php">
            <input type="submit" value="Volver al formulario" />
        </form> 

    </body>

</html>
