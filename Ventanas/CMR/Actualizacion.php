<!DOCTYPE html>
<html>

    <head>

        <meta charset="UTF-8">
        <!--Titulo de la pagina-->
        <title>Actulizar Datos de Cosumo de Materiales por Recurso </title>
       
    </head>

    <body>

        <!--Bloque de php-->
        <?php

            //hace llamado a la conexion de la base datos y al archivo donde se procesan los datos 
            include "Conexion_BD.php";
            require_once "Actualizar_DCMR.php";

        ?>

        <!--Boton que al darle click te devuelve al formulario-->
        <form action="CMR.php">
            <input type="submit" value="Volver al formulario" />
        </form>    

        <!--Botón que al darle click te devuelve al listados de datos-->
        <form action="Listado.php">
            <input type="submit" value="Volver al listado" />
        </form>      
    
    </body>

</html>

