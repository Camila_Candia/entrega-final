<!DOCTYPE html>
<html>
    
    <head>
    
        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Lista de Datos de Consumo de Materiales por Recurso</title>
    
    </head>

    <body>

        <!--Bloque de php que hace el llamdo del archivo donde se realiza el proceso de eliminar datos-->
        <?php

            //hace el llamado a la conexión de la base de datos
            include "Conexion_BD.php";

            //se pregunta si hay datos relacionados con la id y entra al ciclo
            if (isset($_GET['id']))
            {
                //hace el llamado donde se hace el proceso de eliminar datos
                include "Eliminar_DCMR.php";

            }

        ?>

        <table>
        
            <tr>
                <!--Titulos de la lista-->
                <th>Fecha</th>
                <th>Recurso</th>
                <th>Tecnologia</th>
                <th>OT</th>
                <th>Tipo de Tarea</th>
                <th>Nombre</th>
                <th>Código</th>
                <th>Cantidad</th>
            </tr>
         
            <tbody>

                <!--Bloque php que hace llamado a la conexion de la base datos y al archivo donde se procesan los datos para mostarlos en pantalla-->
                <?php

                    //hace llamado al archivo donde se procesan los datos 
                    include "Conexion_BD.php";
                    include "Selecionar_DCMR.php";

                ?>
            
            </tbody>   
            
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="CMR.php">
            <input type="submit" value="Volver al formulario" />
        </form>   
        <a href="Reporte_word.php" >Reporte en Word</a>
            <a href="Reporte_excel.php" >Reporte en Excel</a>

    
    </body>

</html>

