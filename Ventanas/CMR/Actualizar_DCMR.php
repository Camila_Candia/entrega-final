<?php

    //Actualizar los datos 
    //pregunta si los campos estan vacios si no lo estan entran al ciclo 
    if (isset($_POST['Fecha']) and isset($_POST['Recurso']))
    {
        //se capturan todos los datos que 
        $id=$_POST['id'];//la id se incrementa sola
        $fecha=$_POST['Fecha'];
        $recurso=$_POST['Recurso'];
        $tecnologia=$_POST['Tecnología'];
        $ot=$_POST['OT'];
        $tipo_tarea=$_POST['Tipo_de_Tarea'];

        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se hace la consulta sql para la tabla consumo de materiales por recurso
        $consulta_1=$conexion->prepare( "UPDATE cmr SET fecha=:fecha,recurso=:recurso,tecnologia=:tecnologia,ot=:ot,tipo_de_tarea=:t_tarea WHERE id_cmr=:id");

        //se introducen los nuevos datos
        $consulta_1->bindParam(":fecha",$fecha);
        $consulta_1->bindParam(":recurso",$recurso);
        $consulta_1->bindParam(":tecnologia",$tecnologia);
        $consulta_1->bindParam(":ot",$ot);
        $consulta_1->bindParam(":t_tarea",$tipo_tarea);
        $consulta_1->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_1->execute();

        //capturamos los datos que inserta el usuario
        $nombre=$_POST['Nombre'];
        $codigo=$_POST['Código'];
        $cantidad=$_POST['Cantidad'];

        //se hace la conexion de la base datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
        //se hace la consulta sql para la tabla consumo de materiale por recurso detalle
        $consulta_2=$conexion->prepare("UPDATE cmr_det SET nombre=:nombre,codigo=:codigo,cantidad=:cantidad WHERE id_cmr=:id");
        
        //se introducen los nuevos datos
        $consulta_2->bindParam(":nombre",$nombre);
        $consulta_2->bindParam(":codigo",$codigo);
        $consulta_2->bindParam(":cantidad", $cantidad);
        $consulta_2->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_2->execute();

    }

    //Recuperar los datos
    //se preguntan si la id no esta vacia, si no esta vacia entra al ciclo
    if(isset($_GET['id']))
    {
        $id=$_GET['id'];

        // se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se seleccionan todos los datos de la tabla consumo de materiales por recurso 
        $consulta_1=$conexion->prepare("SELECT * FROM cmr WHERE id_cmr=:id");
           
        //se recuperan todos los datos de la tabla consumo de materiales por recurso 
        $consulta_1->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_1->execute();

        //se seleccionan todos los datos de la tabla consumo de materiales por recurso detalle
        $consulta_2=$conexion->prepare("SELECT * FROM cmr_det WHERE id_cmr=:id2");

        //se recuperan todos los datos de la tabla consumo de materiales por recurso detalle
        $consulta_2->bindParam(":id2",$id);

        //ejecutamos la consulta
        $consulta_2->execute();

        //se pregunta si las tablas estan vacias si no la estan ingresan al ciclo
        if($consulta_1->rowCount()>=1 and $consulta_2->rowCount()>=1);
        { 

            //se ponen en un array asociativo para poder recorrerlas 
            $cmr=$consulta_1->fetch();//en cmr estan todos los datos de la tabla consumo de materiales por recurso
            $cmr_det=$consulta_2->fetch();//cmr_det estan todos los da la tabla consumo de materiale por recurso detalle 

            //se muestra el formulario en pantalla para actualizacion 
            echo    '<form action="" method="POST">
                        <!--Se trae el id de la tabla consumo de materiales por recurso pero no se muestra en pantalla-->
                        <input type="hidden" name="id" value="'.$cmr['id_cmr'].'">

                        <table>

                            <tr>    
                                <td>Fecha:</td>
                                <td><input type="date" name="Fecha" value="'.$cmr['fecha'].'" placeholder="dd/mm/aa" required autofocus></td>
                            </tr>  
                            
                            <tr>
                                <td>Recurso:</td>
                                <td><input type="number" name="Recurso" value="'.$cmr['recurso'].'" placeholder="Número de Recurso" required></td>
                            </tr>

                            <tr>  
                                <td>Tecnología:</td>
                                <td><input type="text" name="Tecnología" value="'.$cmr['tecnologia'].'" placeholder="Tipo de Tecnología"></td>
                            </tr>

                            <tr>                    
                                <td>OT</td>
                                <td><input type="number" name="OT" value="'.$cmr['ot'].'" placeholder="Número de OT" required></td>
                            <tr>
                                <td>Tipo de Tarea</td>
                                <td><input type="text" name="Tipo_de_Tarea" value="'.$cmr['tipo_de_tarea'].'" placeholder="Tipo de Tarea"></td>   
                            </tr>
                            
                            <input type="hidden" name="id" value="'.$cmr_det['id_cmr'].'">
                            <tr>
                                <th>Nombre</th>
                                <th>Código</th>
                                <th>Cantidad</th>
                            </tr>
                            
                            <table>

                                <tr>
                                    <td><input type="text" name="Nombre" value="'.$cmr_det['nombre'].'" placeholder="Nombre"></td>
                                    <td><input type="text" name="Código" value="'.$cmr_det['codigo'].'" placeholder="Código"></td> 
                                    <td><input type="number" name="Cantidad" value="'.$cmr_det['cantidad'].'" placeholder="Cantidad"></td>       
                                </tr>

                            </table>
                            
                            <tr>
                                <td><input type="submit" value="Guardar"></td>
                                <td><input type="reset" value="Borrar"></td>
                            </tr>
                            
                        </table>
                    
                    </form>';
        
        } 
    
    }
    else
    {

        echo "Error no se pudo procesar la solicitud";
    
    }

?>
