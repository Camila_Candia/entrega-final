<?php
    //PROCESO PARA INSERTAR LOS DATOS EN LA BASE DATOS 

    //conexion a la base de datos 
    $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
    //captura de datos que el usuario introduce en el formulario 
    $fecha=$_POST['Fecha'];
    $recurso=$_POST['Recurso'];
    $tecnologia=$_POST['Tecnología'];
    $ot=$_POST['OT'];
    $tipo_de_tarea=$_POST['Tipo_de_Tarea'];
    $hoy = date("Y:m:d:h:i:sa");

    //sentencia sql que inserta los datos en la tabla de consumo de materiales por recurso 
    $consulta_1= "INSERT INTO cmr (fecha,recurso,tecnologia,ot,tipo_de_tarea,fecha_creacion) VALUES (?,?,?,?,?,?) RETURNING id_cmr";
    $sql_1=$conexion->prepare($consulta_1);

    //se inseretan los datos en la tabla de consumo de materiales por recurso
    $sql_1->bindParam(1,$fecha);
    $sql_1->bindParam(2,$recurso);
    $sql_1->bindParam(3,$tecnologia);
    $sql_1->bindParam(4,$ot);
    $sql_1->bindParam(5,$tipo_de_tarea);
    $sql_1->bindParam(6,$hoy);
    
    //ejecutamos la sentencia
    $sql_1->execute();

    //los datos que se insertaron en la tabla se cola en un array asociativo
    $id=$sql_1->fetch(PDO::FETCH_ASSOC);
    
    //se pone dentro de una variable la id de la tabla de consmo de materiales por recurso que teambien esta presente en la tabla de consumo de materiales por recurso
    $id_cmr=$id["id_cmr"];

    //si existe un error imprime lo siguiente 
    //echo "Ocurrio un error en la tabla de consumo de materiales por recurso";
        
    //captura de datos que el usuario introduce en el formulario 
    $nombre=$_POST['Nombre'];
    $codigo= $_POST['Código'];
    $cantidad= $_POST['Cantidad'];

    //sentencia sql que inserta los datos en la tabla de consumo de materiales por recurso 
    $consulta_2= "INSERT INTO cmr_det (id_cmr,nombre,codigo,cantidad) VALUES (?,?,?,?)";
    $sql_2= $conexion->prepare($consulta_2);

    //se insertan los datos en la tabla de cosnumo de materiales por recurso detalle
    $sql_2->bindParam(1,$id_cmr);        
    $sql_2->bindParam(2,$nombre);
    $sql_2->bindParam(3,$codigo);
    $sql_2->bindParam(4,$cantidad);
        
    //ejecutamos la sentencia
    $sql_2->execute();

    //Si ocurre un error imprime el siguiente mensaje
    //echo 'Ocurrio un error en la de cosumo de materiales por recurso';
   
    //recarga la pagina despues de darle el boton guaradar 
    header("Location: CMR.php");

?>
