<?php

    //Actualizar los datos 
    //pregunta si los campos estan vacios si no lo estan entran al ciclo 
    if (isset($_POST['Fecha']) and isset($_POST['Recurso']))
    {

        //se capturan todos los datos que inserta el usuario 
        $id=$_POST['id'];// la id se incrementa sola
        $fecha=$_POST['Fecha'];
        $recurso=$_POST['Recurso'];
        $tipo_recurso=$_POST['Tipo_de_Recurso'];

        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se prepara los datos de la tabla salida de materiales por recurso para su actualizacion
        $consulta_1=$conexion->prepare("UPDATE smr SET fecha=:fecha,recurso=:recurso,tipo_de_recurso=:t_recurso WHERE id_smr=:id");

        //se introducen los nuevos datos
        $consulta_1->bindParam(":fecha", $fecha);
        $consulta_1->bindParam(":recurso",$recurso);
        $consulta_1->bindParam(":t_recurso", $tipo_recurso);
        $consulta_1->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_1->execute();
        
        //se captura los datos
        $nombre=$_POST['Nombre'];
        $codigo=$_POST['Código'];
        $cantidad=$_POST['Cantidad'];

        //se hace la conexion de la base datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
        //se prepara los datos de la tabla salida de materiales por recurso detalle para su actualizacion 
        $consulta_2=$conexion->prepare("UPDATE smr_det SET nombre=:nombre, codigo=:codigo, cantidad=:cantidad WHERE id_smr=:id");
        
        //se introducen los nuevos datos
        $consulta_2->bindParam(":nombre",$nombre);
        $consulta_2->bindParam(":codigo",$codigo);
        $consulta_2->bindParam(":cantidad", $cantidad);
        $consulta_2->bindParam(":id",$id);

        //ejectuamos la consulta
        $consulta_2->execute();

    }

    //Recuperar los datos
    //se pregunta si la id no esta vacia, si no lo esta entra en el ciclo y realiza la recupueración de los datos y los muestra en pantalla
    if(isset($_GET['id']))
    {
        
        $id=$_GET['id'];
        
        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se seleccionan todos los datos de la tabla salidad de materiales por recurso 
        $consulta_1=$conexion->prepare("SELECT * FROM smr WHERE id_smr=:id");
       
        //se recuperan todos los datos de la tabla salida de materiale por recurso
        $consulta_1->bindParam(":id",$id);

        //ejecutamos la cosnulta
        $consulta_1->execute();

        //se seleccionan todos los datos de la tabla salida de materiales por recurso detalle
        $consulta_2=$conexion->prepare("SELECT * FROM smr_det WHERE id_smr=:id2");

        //se recuperan todod los datos de la tabla salida de materiales por recurso detalle
        $consulta_2->bindParam(":id2",$id);

        //ejecutamos la consulta
        $consulta_2->execute();

        //se pregunta si las tablas estan vacias si no la estan ingresan al ciclo
        if($consulta_1->rowCount()>=1 and $consulta_2->rowCount()>=1);
        { 

            //se ponen en un array asociativo para poder recorrerlas 
            $smr=$consulta_1->fetch();//en smr estan todos los datos de la tabla salida de materiale por recurso
            $smr_det=$consulta_2->fetch();//en smr_det estan todos los datos de la tabla salida de materiales por recurso detalle

            //se muestra el formulario en pantalla para actualizacion 
            echo   '<form action="" method="POST">
                        <!--Se trae el id de la tabla de salida de materiales por recurso pero no se muestra en pantalla-->
                        <input type="hidden" name="id" value="'.$smr['id_smr'].'">

                        <table>

                            <tr>    
                                <td>Fecha:</td>
                                <td><input type="date" name="Fecha" value="'.$smr['fecha'].'" placeholder="dd/mm/aa" required autofocus></td>
                            </tr>  
                            
                            <tr>
                                <td>Recurso:</td>
                                <td><input type="number" name="Recurso" value="'.$smr['recurso'].'" placeholder="Número de Recurso" required></td>
                            </tr>
        
                            <tr>
                                <td>Tipo de Recurso</td>
                                <td><input type="text" name="Tipo_de_Recurso" value="'.$smr['tipo_de_recurso'].'" placeholder="Tipo de Tarea"></td>   
                            </tr>
                            
                            <input type="hidden" name="id" value="'.$smr_det['id_smr'].'">
                            <tr>
                                <th>Nombre</th>
                                <th>Código</th>
                                <th>Cantidad</th>
                            </tr>
                            
                            <table>

                                <tr>
                                    <td><input type="text" name="Nombre" value="'.$smr_det['nombre'].'" placeholder="Nombre"></td>
                                    <td><input type="text" name="Código" value="'.$smr_det['codigo'].'" placeholder="Código"></td> 
                                    <td><input type="number" name="Cantidad" value="'.$smr_det['cantidad'].'" placeholder="Cantidad"></td>                            
                                </tr>

                            </table>
                            
                            <tr>
                                <td><input type="submit" value="Guardar"></td>
                                <td><input type="reset" value="Borrar"></td>
                            </tr>
                            
                        </table>
                
                    </form>';

        } 
        
    }
    else
    {

        echo "Error no se pudo procesar la solicitud";
    
    }

?>
