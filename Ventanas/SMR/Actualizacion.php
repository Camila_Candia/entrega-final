<!DOCTYPE html>
<html lang="es">
    
    <head>

        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Actualizar Datos de Salida de Materiales por Recurso</title>
       
    </head>
    
    <body>
        
        <!--Bloque php-->
        <?php
          
            //hace llamado a la conexion de la base datos y al archivo donde se procesan los datos 
            include "Conexion_BD.php";
            require_once "Actualizar_DSMR.php";
        ?>

        <!--Boton que al darle click te devuelve al formulario-->
        <form action="SMR.php">
            <input type="submit" value="Volver al formulario" />
        </form>   

        <!--Botón que al darle click te devuelve al listados de datos-->
        <form action="Listado.php">
            <input type="submit" value="Volver al listado" />
        </form>  
    
    </body>

</html>

