
<!DOCTYPE html>
<html lang="es">

    <head>

        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Lista de Datos de Salida de Materiales por recurso</title>

    </head>

    <body>

        <!--Bloque de php en donde se hace el llamado del archivo dodnde se realiza el proceso de eliminar datos-->
        <?php

            //se hace el llamado de la conexion de la base de datos
            include "Conexion_BD.php";

            //se pregunta si hay datos relacionados con la id y entra en el ciclo
            if (isset($_GET['id']))
            {

                //hace el llamdo domde se hace el proceso de eliminar los datos
                include "Eliminar_DSMR.php";

            }

        ?>

        <table>

            <tr>
                <!--Titulos de la lista-->
                <th>Fecha</th>
                <th>Recurso</th>
                <th>Tipo de Recurso</th>
                <th>Nombre</th>
                <th>Código</th>
                <th>Cantidad</th>
            </tr>

            <tbody>

         
                <?php

                    //hace llamado al archivo donde se procesan los datos 
                    include "Conexion_BD.php";
                    include "Selecionar_DSMR.php";
                
                ?>
        
            </tbody>   
        
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="SMR.php">
            <input type="submit" value="Volver al formulario" />
        </form> 
        
        <a href="Reporte_word.php" >Reporte en Word</a>
            <a href="Reporte_excel.php" >Reporte en Excel</a>
    

    </body>

</html>