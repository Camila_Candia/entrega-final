<!DOCTYPE html>
<html lang="es">

    <head>

        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Lista de Datos de Salida de Materiales por recurso</title>

    </head>

    <body>


        <table>

            <tr>
                <!--Titulos de la lista-->
                <th>Fecha</th>
                <th>Recurso</th>
                <th>Tipo de Recurso</th>
                <th>Nombre</th>
                <th>Código</th>
                <th>Cantidad</th>
            </tr>

            <tbody>

         
                <?php

                    //hace llamado al archivo donde se procesan los datos 
                    include "Conexion_BD.php";
                    include "Reporte_e.php";
                
                ?>
        
            </tbody>   
        
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="SMR.php">
            <input type="submit" value="Volver al formulario" />
        </form>  

    </body>

</html>
