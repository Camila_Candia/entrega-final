<?php

    //conexion a la base de datos 
    $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
    //captura de datos que el usuario introduce en el formulario 
    $fecha= $_POST['Fecha'];
    $recurso= $_POST['Recurso'];
    $tipo_de_recurso= $_POST['Tipo_de_Recurso'];
    $hoy = date("Y:m:h:d:i:sa");

    //sentencia sql que prepara la base de datos para inseratr los datos en la tabla de salida de materiales por recurso 
    $consulta_1="INSERT INTO smr (fecha,recurso,tipo_de_recurso,fecha_creacion) VALUES (?,?,?,?) RETURNING id_smr";
    $sql_1=$conexion->prepare($consulta_1);

    //se insertan los en la tabla de salida de materiales por recurso
    $sql_1->bindParam(1,$fecha);
    $sql_1->bindParam(2,$recurso);
    $sql_1->bindParam(3,$tipo_de_recurso);
    $sql_1->bindParam(4,$hoy);
        
    //ejecutamos la consulta
    $sql_1->execute();

    //los datos que se insertaron en la tabla se cola en un array asociativo
    $id=$sql_1->fetch(PDO::FETCH_ASSOC);
    
    //se pone dentro de una variable la id de la salida de materiales por recurso que esta presente tambien en la tabla de salida de materiales por recurso detalle
    $id_smr=$id["id_smr"];
        
    //Si ocurre un error se imprime lo siguiente
    //echo "Ocurrio un error en la tabla de salidad de materiales por recurso" ;
        
    //captura de datos que el usuario introduce en el formulario 
    $nombre=$_POST['Nombre'];
    $codigo=$_POST['Código'];
    $cantidad=$_POST['Cantidad'];

    //sentencia sql que prepara la base de datos para insettar los datos en la tabla de salida de materiales por recurso  detalle
    $consulta_2="INSERT INTO smr_det (id_smr,nombre,codigo,cantidad) VALUES (?,?,?,?)";
    $sql_2=$conexion->prepare($consulta_2);

    //se insetran los datos en la tabla de salidad de materiles por recurso detalle
    $sql_2->bindParam(1,$id_smr);   
    $sql_2->bindParam(2,$nombre);
    $sql_2->bindParam(3,$codigo);
    $sql_2->bindParam(4,$cantidad);

    //ejecutamos la consulta
    $sql_2->execute();

    //si ocurre un error imprime lo siguiente
    //echo 'Ocurrio un error en la segunda tabla';

    //recarga la pagina despues de darle el boton guaradar 
    header("Location: SMR.php");

?>
