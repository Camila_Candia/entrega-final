<?php

   //conexion a la base de datos 
   $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
 
   //sentencia sql donde se solicita los datos que existen la tabla equipos retirados
   $consulta_1=$conexion->prepare( "SELECT id_er,fecha,recurso,partner FROM equipos_retirados");

   //ejecutamos la consulta
   $consulta_1->execute();

   //sentencia donde se solicita los datas que existen la tabla equipos retirados detalle
   $consulta_2=$conexion->prepare("SELECT modelo,numero_de_serial,numero_de_tarjeta,u_a,tipo,tipo_de_tarea,falla,ot,ab FROM equipos_retirados_det");

   //ejecutamos la consulta
   $consulta_2->execute();

   //se cuenta cuantas filas exiten cada tabla si es mayor a 1  entra en el ciclo mientras
   if($consulta_1->rowCount()>=1 and $consulta_2->rowCount()>=1)
   {

      //se crea un array asociativo de las dos tablas para recorrerlas 
      while($er=$consulta_1->fetch() and $er_det=$consulta_2->fetch())
      {
         
         //se imprime en pantalla lo que existen en las tablas 
         echo "<tr> 
                  <td>".$er['fecha']."</td>
                  <td>".$er['recurso']."</td>
                  <td>".$er['partner']."</td>
                  <td>".$er_det['modelo']."</td>
                  <td>".$er_det['numero_de_serial']."</td>
                  <td>".$er_det['numero_de_tarjeta']."</td>
                  <td>".$er_det['u_a']."</td>
                  <td>".$er_det['tipo']."</td>
                  <td>".$er_det['tipo_de_tarea']."</td>
                  <td>".$er_det['falla']."</td>
                  <td>".$er_det['ot']."</td>
                  <td>".$er_det['ab']."</td>
                  <td><a href='Actualizacion.php?id=".$er['id_er']."'> Actualizar Datos</a></td>
                  <td><a href='Listado.php?id=".$er['id_er']."'> Eliminar Datos</a></td>
               </tr>";

      }
      
   } 
   else 
   {
      
      echo "No existe ningun dato";
   
   }

?>
