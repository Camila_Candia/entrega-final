<?php

    //Actualizar los datos 
    //pregunta si los campos estan vacios si no lo estan entran al ciclo 
    if (isset($_POST['Fecha']) and isset($_POST['Recurso']))
    {

        //se capturan todos los datos que introduce el usuario en el fomulario 
        $id=$_POST['id'];// la id se incrementa sola
        $fecha=$_POST['Fecha'];
        $recurso=$_POST['Recurso'];
        $partner=$_POST['Partner'];

        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se prepara la base datos para actualizar los datos en la tabla equipos retirados
        $consulta_1=$conexion->prepare("UPDATE equipos_retirados SET fecha=:fecha,recurso=:recurso,partner=:partner WHERE id_er=:id");

        //se introducen los nuevos datos
        $consulta_1->bindParam(":fecha",$fecha);
        $consulta_1->bindParam(":recurso",$recurso);
        $consulta_1->bindParam(":partner", $partner);
        $consulta_1->bindParam(":id",$id);


        //ejecutamos la consulta
        $consulta_1->execute();

        //se capturan los datos que introduce el usuario en el formulario
        $modelo=$_POST['Modelo'];
        $n_serial=$_POST['N_Serial'];
        $n_tarjeta=$_POST['N_Tarjeta'];
        $u_a=$_POST['U/A'];
        $tipo=$_POST['Tipo'];
        $tipo_tarea=$_POST['Tipo_Tarea'];
        $falla=$_POST['Falla'];
        $ot=$_POST['OT'];
        $ab=$_POST['AB'];

        //se hace la conexion de la base datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se prepara la base de datos para actualizar los datos de la tabla equipos retirados detalle
        $consulta_2=$conexion->prepare("UPDATE equipos_retirados_det SET modelo=:modelo,numero_de_serial=:n_serial,numero_de_tarjeta=:n_tarjeta,u_a=:u_a,tipo=:tipo,tipo_de_tarea=:t_tarea,falla=:falla,ot=:ot,ab=:ab WHERE id_er=:id");
       
        //se introducen los nuevos datos
        $consulta_2->bindParam(":modelo",$modelo);
        $consulta_2->bindParam(":n_serial",$n_serial);
        $consulta_2->bindParam(":n_tarjeta", $n_tarjeta);
        $consulta_2->bindParam(":u_a",$u_a);
        $consulta_2->bindParam(":tipo",$tipo);
        $consulta_2->bindParam(":t_tarea", $tipo_tarea);
        $consulta_2->bindParam(":falla",$falla);
        $consulta_2->bindParam(":ot",$ot);
        $consulta_2->bindParam(":ab", $ab);
        $consulta_2->bindParam(":id",$id);

        //ejectamos la consulta
        $consulta_2->execute();

    }

    //Recuperar los datos
    //se preguntan si la id no esta vacia, si no esta vacia entra al ciclo
    if(isset($_GET['id']))
    {
        
        $id=$_GET['id'];
        
        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se seleccionan todos los datos de la tabla equipos retirados  
        $consulta_1=$conexion->prepare("SELECT * FROM equipos_retirados WHERE id_er=:id");
    
        //se recuperen todos los datos de la tabla equipos retirados
        $consulta_1->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_1->execute();

        //se seleccionan todos los datos de la tabla equipos retidos detalle
        $consulta_2=$conexion->prepare("SELECT * FROM equipos_retirados_det WHERE id_er=:id2");

        //se recuperan todos los datos de la tabla equipos retirados detalle
        $consulta_2->bindParam(":id2",$id);

        //ejecutamos
        $consulta_2->execute();

        //se pregunta si las tablas estan vacias si no la estan ingresan al ciclo
        if($consulta_1->rowCount()>=1 and $consulta_2->rowCount()>=1);
        { 

            //se ponen en un array asociativo para poder recorrerlas 
            $er=$consulta_1->fetch();//en er estan todos los datos de la tabla equipos retirados 
            $er_det=$consulta_2->fetch();//en er_det estan todos los datos de la tabla equipos retirados detalle

            //se muestra el formulario en pantalla para actualizacion 
            echo   '<form action="" method="POST">
                        <!--Se trae el id de la tabla consumo de materiales por recurso pero no se muestra en pantalla-->
                        <input type="hidden" name="id" value="'.$er['id_er'].'">

                        <table>

                            <tr>    
                                <td>Fecha:</td>
                                <td><input type="date" name="Fecha" value="'.$er['fecha'].'" placeholder="dd/mm/aa" required autofocus></td>
                            </tr>  
                            
                            <tr>
                                <td>Recurso:</td>
                                <td><input type="number" name="Recurso" value="'.$er['recurso'].'" placeholder="Número de Recurso" required></td>
                            </tr>

                            <tr>  
                                <td>Partner:</td>
                                <td><input type="text" name="Partner" value="'.$er['partner'].'" placeholder="Partner"></td>
                            </tr>
                            
                            <input type="hidden" name="id" value="'.$er_det['id_er'].'">
                            <tr>
                                    <th>Modelo</th>
                                    <th>N° de Serial</th>
                                    <th>N° de Tarjeta</th>
                                    <th>U/A</th>
                                    <th>Tipo</th>
                                    <th>Tipo de Tarea</th>
                                    <th>Falla</th>
                                    <th>OT</th>
                                    <th>AB</th>
                            </tr>
                            
                            <table>
                                
                                <tr>
                                    <td><input type="text" name="Modelo" value="'.$er_det['modelo'].'" placeholder="Modelo"></td>
                                    <td><input type="text" name="N_Serial" value="'.$er_det['numero_de_serial'].'" placeholder="N° de Serial"></td> 
                                    <td><input type="text" name="N_Tarjeta" value="'.$er_det['numero_de_tarjeta'].'" placeholder="N° de Trajeta"></td>
                                    <td><input type="text" name="U/A" value="'.$er_det['u_a'].'" placeholder="U/A"></td>
                                    <td><input type="text" name="Tipo" value="'.$er_det['tipo'].'" placeholder="Tipo de decodificador"></td> 
                                    <td><input type="text" name="Tipo_Tarea" value="'.$er_det['tipo_de_tarea'].'" placeholder="Tipo de Tarea"></td>
                                    <td><input type="text" name="Falla" value="'.$er_det['falla'].'" placeholder="Falla"></td>
                                    <td><input type="number" name="OT" value="'.$er_det['ot'].'" placeholder="Orden de Trabajo"></td> 
                                    <td><input type="number" name="AB" value="'.$er_det['ab'].'" placeholder="Abonado"></td>     
                                </tr>

                            </table>
                            
                            <tr>
                                <td><input type="submit" value="Guardar"></td>
                                <td><input type="reset" value="Borrar"></td>
                            </tr>
                                                   
                        </table>

                    </form>';

        } 
        
    }
    else
    {

        echo "Error no se pudo procesar la solicitud";

    }
    
?>
