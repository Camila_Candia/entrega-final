<!DOCTYPE html>
<html>

    <head>

        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Lista de Datos de Equipos Retirados</title>

    </head>


        <table>

            <tr>
                <!--Titulos de la lista-->
                <th>Fecha</th>
                <th>Recurso</th>
                <th>Partner</th>
                <th>Modelo</th>
                <th>N° de Serial</th>
                <th>N° de Tarjeta</th>
                <th>U/A</th>
                <th>Tipo</th>
                <th>Tipo de Tarea</th>
                <th>Falla</th>
                <th>OT<th>
                <th>AB</th>
            </tr>
        
            <tbody>

                <!--Bloque php que hace llamado a la conexion de la base datos y al archivo donde se procesan los datos para mostarlos en pantalla-->
                <?php

                    //hace llamado al archivo donde se procesan los datos 
                    include "Conexion_BD.php";
                    include "Reporte_e.php";
                
                ?>
        
            </tbody>   
            
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="Equipos_Retirados.php">
            <input type="submit" value="Volver al formulario" />
        </form>

    </body>

</html>
