<?php
    //PROCESO DE INSERTAR LOS DATOS EN LA BASE DE DATOS 

    //conexion a la base de datos 
    $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
    //captura de datos que el usuario introduce en el formulario 
    $fecha=$_POST['Fecha'];
    $recurso= $_POST['Recurso'];
    $partner=$_POST['Partner'];
    $hoy = date("Y:m:h:i:sa");

    //sentencia sql que prepara la base de datos para introducir datos en la tabla de equipos retirados 
    $consulta_1="INSERT INTO equipos_retirados (fecha,recurso,partner,fecha_creacion) VALUES (?,?,?,?) RETURNING id_er";
    $sql_1=$conexion->prepare($consulta_1);

    //se insertan los datos en la tabla de equipos retirados 
    $sql_1->bindParam(1,$fecha);
    $sql_1->bindParam(2,$recurso);
    $sql_1->bindParam(3,$partner);
    $sql_1->bindParam(4,$hoy);

    //ejecutamos la consulta
    $sql_1->execute();

    //los datos que se insertaron en la tabla se cola en un array asociativo
    $id=$sql_1->fetch(PDO::FETCH_ASSOC);
    
    //se pone dentro de una variable la id de la tabla de equipos retirados que tambien esta presente en equipos retirados detalle
    $id_er=$id["id_er"];

    //si ocurre un error imprime lo siguiente
    //echo "Ocurrio un error en la tabla de equipos retirados";
        
    //captura de datos que el usuario introduce en el formulario 
    $modelo=$_POST['Modelo'];
    $n_serial=$_POST['N_Serial'];
    $n_tarjeta=$_POST['N_Tarjeta'];
    $u_a=$_POST['U/A'];
    $tipo=$_POST['Tipo'];
    $tipo_tarea=$_POST['Tipo_Tarea'];
    $falla=$_POST['Falla'];
    $ot=$_POST['OT'];
    $ab=$_POST['AB'];

    //sentencia sql que prepara la base de datos para insertar los datos en la tabla de equipos retirados detalle 
    $consulta_2="INSERT INTO equipos_retirados_det (id_er,modelo,numero_de_serial,numero_de_tarjeta,u_a,tipo,tipo_de_tarea,falla,ot,ab) VALUES (?,?,?,?,?,?,?,?,?,?)";
    $sql_2=$conexion->prepare($consulta_2);

    //se insertan los datos en la tabla de equipos retirados detalle
    $sql_2->bindParam(1,$id_er);
    $sql_2->bindParam(2,$modelo);
    $sql_2->bindParam(3,$n_serial);
    $sql_2->bindParam(4,$n_tarjeta);
    $sql_2->bindParam(5,$u_a);
    $sql_2->bindParam(6,$tipo);
    $sql_2->bindParam(7,$tipo_tarea);
    $sql_2->bindParam(8,$falla);
    $sql_2->bindParam(9,$ot);
    $sql_2->bindParam(10,$ab);

    //ejecutamos la tabla
    $sql_2->execute();
    
    //si ocurre imprime lo siguiente
    //echo 'Ocurrio un error en la tabla de equipos retirados detalle';
    
    //recarga la pagina despues de darle el boton guaradar 
    header("Location: Equipos_Retirados.php");

?>
