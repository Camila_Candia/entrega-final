<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <!--El título que aparece en la pestaña-->
        <title>Control de Bobinas por Recurso</title>

        <!--Estilos que se le aplican a la ventana--> 
        <link rel="stylesheet" href="Estilos/estilos.css">

    </head>

    <body>
    <header>
		<nav class="navegacion">
			<ul class="menu">
				<li><a href="#">Administración</a>
					<ul class="submenu">
						<li><a href="../CMR/CMR.php">Consumo de Materiales por Recurso</a></li>
						<li><a href="../CBR/CBR.php">Control de Bobinas por Recurso</a></li>
						<li><a href="../Inventario/Inventario.php">Inventario Semanal Interno</a></li>
					</ul>
				</li>
				<li><a href="#">Depósito</a>
					<ul class="submenu">
						<li><a href="../Asignar_Equipos/Asignar_Equipos.php">Asignación de Equipos</a></li>
						<li><a href="../Movimientos/Movimientos.php">Movimientos</a></li>
						<li><a href="../SMR/SMR.php">Salida de Materiales por Recurso</a></li>
					</ul>
				</li>
				<li><a href="../Equipos_Retirados/Equipos_Retirados.php">Mantenimiento</a></li>
				<li><a href="#">Reportes</a></li>
			</ul>
        </nav>
	</header>

        <!--Título principal-->
        <h1>Control de Bobinas por Recurso</h1>

        <div id="main">
            <!--Formulario donde el usuario caraga los datos de los materiales que los tenicos utilizan-->
            <form action="Datos_CBR.php" method="POST">

                <table>
                    
                    <tr>
                        <td>Recurso:</td>
                        <td> 
                            <select name="Recurso">

                                <?php

                                    //incluimos el archivo donde se hace el proceso de mostras los depositos exstentes
                                    include "../../Depositos.php";

                                ?>
                            </select>
                            <!--<input type="number" name="Recurso" placeholder="Número de Recurso" required>-->
                        </td>
                    </tr>

                    <tr>
                        <th>Incicio</th>
                        <th>Fin</th>
                        <th>Fecha de Entrega</th>
                        <th>Fecha de Devolucion</th>
                        <th>Comentarios</th>
                    </tr>
                    
                    <table>
    
                        <tr>
                            <td><input type="number" name="Inicio" placeholder="Inicio del Cable"></td>
                            <td><input type="number" name="Fin" placeholder="Fin del Cable"></td> 
                            <td><input type="date" name="Fecha_E" placeholder="Fecha de Entrega"></td>
                            <td><input type="date" name="Fecha_D" placeholder="Fecha de Devolción"></td>
                            <td><input type="text" name="Comentario" placeholder="Comentario"></td>
                        </tr>
    
                    </table>
                    
                    <tr>
                        <td><input type="submit" value="Guardar"></td>
                        <td><input type="reset" value="Borrar"></td>
                    </tr>
            
                </table>
    
            </form>

            <!--Botones que hay que revisar si es que se puede mejorar-->
            
            <!--Boton salir, envia a la pagina del menu-->
            <form action="../../index.php">
                <input type="submit" value="Salir" />
            </form>    
            
            <!--Boton que muestra lo que se cargo en la tabla--> 
            <form action="Listado.php">
                <input type="submit" value="Listado de Datos"/>
            </form>

            
       
        </div>

    </body>

</html>
