<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Actualización de Datos de Control de Bobinas por Recurso</title>
       
    </head>

    <body>

        <!--bloque de php-->
        <?php

            //hace llamado a la conexion de la base datos y al archivo donde se procesan los datos 
            include "Conexion_BD.php";
            require_once "Actualizar_DCBR.php";
        
        ?>

        <!--Boton que al darle click te devuelve al formulario-->
        <form action="CBR.php">
            <input type="submit" value="Volver al formulario" />
        </form>  

        <!--Botón que al darle click te devuelve al listados de datos-->
        <form action="Listado.php">
            <input type="submit" value="Volver al listado" />
        </form>    
    
    </body>

</html>

