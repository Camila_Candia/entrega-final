<?php

    //Actualizar los datos 
    //pregunta si los campos estan vacios si no lo estan entran al ciclo 
    if ( isset($_POST['Recurso']))
    {

        //se capturan todos los datos que 
        $id=$_POST['id'];// la id se incrementa sola
        $recurso=$_POST['Recurso'];

        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se hace la consulta sql para la tabla control de bobinas
        $consulta_1=$conexion->prepare("UPDATE cbr SET recurso=:recurso WHERE id_cbr=:id");

        //se introducen los nuevos datos
        $consulta_1->bindParam(":recurso",$recurso);
        $consulta_1->bindParam(":id",$id);

        //ejecutamos la consulta 
        $consulta_1->execute();

        //se captura los datos que inserta el usuario
        $inicio=$_POST['Inicio'];
        $fin=$_POST['Fin'];
        $fecha_e=$_POST['Fecha_E'];
        $fecha_d=$_POST['Fecha_D'];
        $comentario=$_POST['Comentario'];

        //se hace la conexion de la base datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
        //se hace la consulta sql para la tabla de control de bobina detalle
        $consulta_2=$conexion->prepare("UPDATE cbr_det SET inicio=:inicio,final=:fin,fecha_de_entrega=:fecha_e,fecha_de_devolucion=:fecha_d,comentario=:comentario  WHERE id_cbr=:id");
        
        //se introducen los nuevos datos
        $consulta_2->bindParam(":inicio",$inicio);
        $consulta_2->bindParam(":fin",$fin);
        $consulta_2->bindParam(":fecha_e", $fecha_e);
        $consulta_2->bindParam(":fecha_d",$fecha_d);
        $consulta_2->bindParam(":comentario",$comentario);
        $consulta_2->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_2->execute();

    }

    //Recuperar los datos
    //se pregunta si la id no esta vacia, si no lo esta entre en el ciclo y realiza la recuperacion de los datos y los muetra en pantalla
    if(isset($_GET['id']))
    {
        $id=$_GET['id'];

        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se seleccionan todos los datos de la tabla control de bobinas por recurso
        $consulta_1=$conexion->prepare("SELECT * FROM cbr WHERE id_cbr=:id");
        
        //se recuperan todos los datos de la tabla control de bobinas por recurso
        $consulta_1->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_1->execute();

        //se recuperan los datos de la tabla control de bobinas por recurso detalle
        $consulta_2=$conexion->prepare("SELECT * FROM cbr_det WHERE id_cbr=:id2");

        //se recuperan los datos de la tabla control de bobinas por detalle
        $consulta_2->bindParam(":id2",$id);

        //ejecutamos las consultas
        $consulta_2->execute();

        //se pregunta si las tablas estan vacias si no la estan ingresan al ciclo
        if($consulta_1->rowCount()>=1 and $consulta_2->rowCount()>=1);
        { 
            
            //se ponen en un array asociativo para poder recorrerlas 
            $cbr=$consulta_1->fetch(); //en cbr estan los datos de la tabla control de bobinas 
            $cbr_det=$consulta_2->fetch();// en cbr_det estan los datos de la tabla control de bobinas detalle
            
            //se muestra el formulario en pantalla para actualizacion 
                echo '<form action="" method="POST">
                            <!--Se trae el id de la tabla control de bobinas pero no se muestra en pantalla-->
                            <input type="hidden" name="id" value="'.$cbr['id_cbr'].'">

                            <table>  
                                
                                <tr>
                                    <td>Recurso:</td>
                                    <td><input type="number" name="Recurso" value="'.$cbr['recurso'].'" placeholder="Número de Recurso" required></td>
                                </tr>
                                
                                <input type="hidden" name="id" value="'.$cbr_det['id_cbr'].'">
                                <tr>
                                    <th>Inicio</th>
                                    <th>Fin</th>
                                    <th>Fecha de Entrega</th>
                                    <th>Fecha de Devolucion</th>
                                    <th>Comentario</th>
                                </tr>
                                
                                <table>

                                    <tr>
                                        <td><input type="number" name="Inicio" value="'.$cbr_det['inicio'].'" placeholder="Incio del Cable"></td>
                                        <td><input type="number" name="Fin" value="'.$cbr_det['final'].'" placeholder="Fin del Cable"></td> 
                                        <td><input type="date" name="Fecha_E" value="'.$cbr_det['fecha_de_entrega'].'" placeholder="Fecha de Entrega"></td>
                                        <td><input type="date" name="Fecha_D" value="'.$cbr_det['fecha_de_devolucion'].'" placeholder="Fecha de Devolución"></td>
                                        <td><input type="text" name="Comentario" value="'.$cbr_det['comentario'].'" placeholder="Comentario"></td>
                                    </tr>

                                </table>
                                
                                <tr>
                                    <td><input type="submit" value="Guardar"></td>
                                    <td><input type="reset" value="Borrar"></td>
                                </tr>
                                
                            </table>
                
                    </form>';
        } 
        
    }
    else
    {

        echo "Error no se pudo procesar la solicitud";
 
    }
?>
