<!DOCTYPE html>
<html lang="es">

    <head>

        <meta charset="UTF-8">
        <!--Título de la página-->
        <title>Listado de Datos</title>
    
    </head>

    <body>

        <table>

            <tr>
                <!--Titulos de la lista-->
                <th>Recurso</th>
                <th>Inicio</th>
                <th>Fin</th>
                <th>Fecha de Entrega</th>
                <th>Fecha de Devolución</th>
                <th>Comentario</th>
            </tr>

            <tbody>

                <!--Bloque php que hace llamdo a la conexion de la base datos y al archivo donde se procesa los datos para mostralos en pantalla-->
                <?php
                    
                    //hace llamado al archivo donde se procesan los datos 
                    include "Conexion_BD.php";
                    include "Reporte_w.php";
                
                ?>
            
            </tbody>   
         
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="CBR.php">
            <input type="submit" value="Volver al formulario" />
        </form>   
         
    
    </body>

</html>

