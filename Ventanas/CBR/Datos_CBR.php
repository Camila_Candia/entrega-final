<?php
    //PROCESO DE INSERTAR DATOS EN LA BASE DE DATOS
    
    //conexion a la base de datos 
    $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
    //captura de datos que el usuario introduce en el formulario 
    $recurso=$_POST['Recurso'];
    $hoy = date("Y:m:d:h:i:sa");

    //sentencia sql que prepara los datos en la tabla de control de bobinas por recurso
    $consulta_1= "INSERT INTO cbr (recurso,fecha_creacion) VALUES (?,?) RETURNING id_cbr";
    $sql_1=$conexion->prepare($consulta_1);
        
    //se insertan los datos en la tabla de control de bobinas por recurso
    $sql_1->bindParam(1,$recurso);
    $sql_1->bindParam(2,$hoy);

    //ejecutamos la sentecia 
    $sql_1->execute();

    //los datos que se insertaron en la tabla se cola en un array asociativo
    $id=$sql_1->fetch(PDO::FETCH_ASSOC);
    
    //se pone dentro de una variable la id de la tabla de control de bobinas por recurso que tambien esta en la tabla de control de bobina spor recurso detalle
    $id_cbr=$id["id_cbr"];

    //si exite un error imprime lo siguiente
    //echo "Ocurrio un error en la de control de bobinas por recurso";
    
    //captura de datos que el usuario introduce en el formulario 
    $inicio=$_POST['Inicio'];
    $fin=$_POST['Fin'];
    $fecha_e=$_POST['Fecha_E'];
    $fecha_d=$_POST['Fecha_D'];
    $comentario=$_POST['Comentario'];

    //sentencia sql que prepara los datos en la tabla de control de bobinas por detalle
    $consulta_2= "INSERT INTO cbr_det (id_cbr,inicio,final,fecha_de_entrega,fecha_de_devolucion,comentario) VALUES (?,?,?,?,?,?)";
    $sql_2=$conexion->prepare($consulta_2);

    //se insertan los datos en la tabla de control de bobinas por recursp detalle
    $sql_2->bindParam(1,$id_cbr);    
    $sql_2->bindParam(2,$inicio);
    $sql_2->bindParam(3,$fin);
    $sql_2->bindParam(4,$fecha_e);
    $sql_2->bindParam(5,$fecha_d);
    $sql_2->bindParam(6,$comentario);
    
    //ejecutamos la sentencia    
    $sql_2->execute();
  
    //Si ocurre un error un imprime lo siguiente
    //echo 'Ocurrio un error en la segunda tabla';
    
    //recarga la pagina despues de darle el boton guaradar 
    header("Location: CBR.php");

?>
