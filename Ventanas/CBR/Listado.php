<!DOCTYPE html>
<html lang="es">

    <head>

        <meta charset="UTF-8">
        <!--Título de la página-->
        <title>Listado de Datos</title>
    
    </head>

    <body>

        <!--Bloque de php donde se hace el llamado del archivo donde se realiza el proceso de eliminar datos-->
        <?php
            
            //hace el llamado de la conexión a la base datos
            include "Conexion_BD.php";

            //se pregunta si hay datos relacionados con la id y entra en el ciclo 
            if (isset($_GET['id']))
            {

                //hac el llamado donde se hace el proceso de eliminar los datos 
                include "Eliminar_DCBR.php";
            }

        ?>

        <table>

            <tr>
                <!--Titulos de la lista-->
                <th>Recurso</th>
                <th>Inicio</th>
                <th>Fin</th>
                <th>Fecha de Entrega</th>
                <th>Fecha de Devolución</th>
                <th>Comentario</th>
            </tr>

            <tbody>

                <!--Bloque php que hace llamdo a la conexion de la base datos y al archivo donde se procesa los datos para mostralos en pantalla-->
                <?php
                    
                    //hace llamado al archivo donde se procesan los datos 
                    include "Conexion_BD.php";
                    include "Selecionar_DCBR.php";
                
                ?>
            
            </tbody>   
         
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="CBR.php">
            <input type="submit" value="Volver al formulario" />
        </form>   
        <a href="Reporte_word.php" >Reporte en Word</a>
            <a href="Reporte_excel.php" >Reporte en Excel</a>


    
    </body>

</html>

