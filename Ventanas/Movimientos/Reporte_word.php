<!DOCTYPE html>
<html lang="es">

    <head>
        
        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Listado de Datos de Movimientos</title>
    
    </head>

    <body>


        <table>

            <tr>
                <!--Titulos de la lista-->
                <th>Fecha</th>
                <th>Deposito de Origen</th>
                <th>Deposito de Destino</th>
                <th>Tipo de Movimiento</th>
                <th>Nombre</th>
                <th>Código</th>
                <th>Cantidad</th>
            </tr>

            <tbody>

                <!--Bloque php en donde se hace el el llamdo a la conexion de la base de datos y el archivo donde se procesa los datos para mostrar los datos en pantalla-->
                <?php

                    //hace llamado al archivo donde se procesan los datos 
                    include "Conexion_BD.php";
                    include "Reporte_w.php";
                
                ?>
        
            </tbody>      
        
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="Movimientos.php">
            <input type="submit" value="Volver al formulario" />
        </form>    
    
    </body>
</html>

