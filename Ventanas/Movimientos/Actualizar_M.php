
<?php
    //Actualizar los datos 
    //pregunta si los campos estan vacios si no lo estan entran al ciclo 
    if (isset($_POST['Fecha']))
    {
        //se capturan todos los datos que inserta el usuario 
        $id=$_POST['id'];// la id se incrementa sola
        $fecha=$_POST['Fecha'];
        $deposito_origen=$_POST['Deposito_Origen'];
        $deposito_destino=$_POST['Deposito_Destino'];
        $tipo_movimiento=$_POST['Tipo_de_Movimiento'];

        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se hace la consulta para la tabla de movimientos
        $consulta_1=$conexion->prepare("UPDATE movimientos SET fecha=:fecha,deposito_origen=:d_origen,deposito_destino=:d_destino,tipo_de_movimiento=:t_movimiento WHERE id_movimiento=:id");

        //se introducen los nuevos datos
        $consulta_1->bindParam(":fecha",$fecha);
        $consulta_1->bindParam(":d_origen",$deposito_origen);
        $consulta_1->bindParam(":d_destino",$deposito_destino);
        $consulta_1->bindParam(":t_movimiento",$tipo_movimiento);
        $consulta_1->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_1->execute();
        
        //se captura los datos que inserta el usuario
        $nombre=$_POST['Nombre'];
        $codigo=$_POST['Código'];
        $cantidad=$_POST['Cantidad'];

        //se hace la conexion de la base datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se hace la consulta sql para la tabla movimientos detalle
        $consulta_2=$conexion->prepare("UPDATE movimientos_det SET nombre=:nombre, codigo=:codigo, cantidad=:cantidad WHERE id_movimiento=:id");
        
        //se introducen los nuevos datos
        $consulta_2->bindParam(":nombre",$nombre);
        $consulta_2->bindParam(":codigo",$codigo);
        $consulta_2->bindParam(":cantidad", $cantidad);
        $consulta_2->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_2->execute();

    }

    //Recuperar los datos
    //se pregunta si la id no esta vacia, si no lo esta entra en el ciclo y realiza la recupueración de los datos y los muestra en pantalla
    if(isset($_GET['id']))
    {

        $id=$_GET['id'];
        
        // se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se seleccionan todos los datos de la yabla movimientos
        $consulta_1=$conexion->prepare("SELECT * FROM movimientos WHERE id_movimiento=:id");
        
        //se recuperan los datos de la tabla movimientos
        $consulta_1->bindParam(":id",$id);

        //se ejecuta la consulata
        $consulta_1->execute();

        //se seleccionan todos los datos de la tabla de movimientos detalle
        $consulta_2=$conexion->prepare("SELECT * FROM movimientos_det WHERE id_movimiento=:id");

        //se recuperan los datos de la tabla movimientos detalle
        $consulta_2->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_2->execute();

        //se pregunta si las tablas estan vacias si no la estan ingresan al ciclo
        if($consulta_1->rowCount()>=1 and $consulta_2->rowCount()>=1);
        { 

            //se ponen en un array asociativo para poder recorrerlas 
            $m=$consulta_1->fetch();//en m estan todos los datos de la tabla movimientos
            $m_det=$consulta_2->fetch();//en m_det estan todod los datos de la tabla movimientos detalle

            //se muestra el formulario en pantalla para actualizacion 
            echo   '<form action="" method="POST">
                        <!--Se trae el id de la tabla de movimiento pero no se muestra en pantalla-->
                        <input type="hidden" name="id" value="'.$m['id_movimiento'].'">

                        <table>

                            <tr>    
                                <td>Fecha:</td>
                                <td><input type="date" name="Fecha" value="'.$m['fecha'].'" placeholder="dd/mm/aa" required autofocus></td>
                            </tr>  
                            
                            <tr>
                                <td>Deposito de Origen</td>
                                <td><input type="text" name="Deposito_Origen" value="'.$m['deposito_origen'].'" placeholder="Deposito de Origen" required></td>
                            </tr>

                            <tr>
                                <td>Deposito de Destino</td>
                                <td><input type="text" name="Deposito_Destino" value="'.$m['deposito_destino'].'" placeholder="Deposito de Destino" required></td>
                            </tr>

                            <tr>
                                <td>Tipo de Movimiento</td>
                                <td><input type="text" name="Tipo_de_Movimiento" value="'.$m['tipo_de_movimiento'].'" placeholder="Tipo de Movimiento" required></td>
                            </tr>

                            <input type="hidden" name="id" value="'.$m_det['id_movimiento'].'">
                            <tr>
                                <th>Nombre</th>
                                <th>Código</th>
                                <th>Cantidad</th>
                            </tr>
                            
                            <table>
                                
                                <tr>
                                    <td><input type="text" name="Nombre" value="'.$m_det['nombre'].'" placeholder="Nombre"></td>
                                    <td><input type="text" name="Código" value="'.$m_det['codigo'].'" placeholder="Código"></td> 
                                    <td><input type="number" name="Cantidad" value="'.$m_det['cantidad'].'" placeholder="Cantidad"></td>    
                                </tr>

                            </table>
                            
                            <tr>
                                <td><input type="submit" value="Guardar"></td>
                                <td><input type="reset" value="Borrar"></td>
                            </tr>
                                
                         </table>

                    </form>';

        } 
    
    }
    else
    {

        echo "Error no se pudo procesar la solicitud";
    
    }
    
?>