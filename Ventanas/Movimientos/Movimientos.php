<!DOCTYPE html>
<html lang="es">

    <head>
        
        <meta charset="UTF-8">
        <!--El título que aparece en la pestaña-->
        <title>Movimientos</title>

        <!--Estilos que se le aplican a la ventana--> 
        <link rel="stylesheet" href="Estilos/estilos.css">

        <script type="text/javascript">

            //funcion para crear la conexion asincronica
            function ListaCodigo(str)
            { 
                var conexion;

                //si la variable a enviar viene vacia retornamos a nada la funcion
                if(str=="")
                {
                    
                    document.getElementById("txtHint").innerHTML=""; 
                    return;
                
                }
                
                // creamos una nueva instacion del obejeto XMLHttpRequest
                if (window.XMLHttpRequest)
                {

                    conexion = new XMLHttpRequest();  
                
                }

                // verificamos el onreadystatechange verifando que el estado sea de 4 y el estatus 200
                conexion.onreadystatechange=function()
                {  

                    if(conexion.readyState==4 && conexion.status==200)
                    {

                        //especificamos que en el elemento HTML cuyo id esa el de "Codigo" vacie todos los datos de la respuesta 
                        document.getElementById("Codigo").innerHTML=conexion.responseText; 
                    
                    }
               
                }

                //abrimos una conexion asincronica usando el metodo GET y le enviamos la variable c
                conexion.open("GET", "../../Codigos.php?m="+str, true);
                
                //por ultimo enviamos la conexion
                conexion.send();

            }
			
	    </script>
    
    </head>

    <body>
    <header>
		<nav class="navegacion">
			<ul class="menu">
				<li><a href="#">Administración</a>
					<ul class="submenu">
						<li><a href="../CMR/CMR.php">Consumo de Materiales por Recurso</a></li>
						<li><a href="../CBR/CBR.php">Control de Bobinas por Recurso</a></li>
						<li><a href="../Inventario/Inventario.php">Inventario Semanal Interno</a></li>
					</ul>
				</li>
				<li><a href="#">Depósito</a>
					<ul class="submenu">
						<li><a href="../Asignar_Equipos/Asignar_Equipos.php">Asignación de Equipos</a></li>
						<li><a href="../Movimientos/Movimientos.php">Movimientos</a></li>
						<li><a href="../SMR/SMR.php">Salida de Materiales por Recurso</a></li>
					</ul>
				</li>
				<li><a href="../Equipos_Retirados/Equipos_Retirados.php">Mantenimiento</a></li>
				<li><a href="#">Reportes</a></li>
			</ul>
        </nav>
	</header>
        <!--Título principal-->
        <h1>Movimientos</h1>

        <div id="main">
            <!--Formulario donde el usuario caraga los datos de los materiales que los tenicos utilizan-->
            <form action="Datos_M.php" method="POST">

                <table>

                    <tr>    
                        <td>Fecha:</td>
                        <td><input type="date" name="Fecha" placeholder="dd/mm/aa" required autofocus></td>
                    </tr>  
                    
                    <tr>
                        <td>Deposito de Origen</td>
                        <td>
                            <select name="Deposito_Origen" id="Deposito">
                            
                                <?php

                                    //incluimos el archivo donde se hace el proceso de mostras los depositos exstentes
                                    include "../../Depositos.php";

                                ?>
                        
                            </select>
                            <!--<input type="text" name="Deposito_Origen" placeholder="Deposito de Origen" required>-->
                        </td>
                    </tr>

                    <tr>
                        <td>Deposito de Destino</td>
                        <td>
                            <select name="Deposito_Destino" id="Deposito">
                            
                                <?php

                                    //incluimos el archivo donde se hace el proceso de mostras los depositos exstentes
                                    include "../../Depositos.php";

                                ?>
                        
                            </select>
                            <!--<input type="text" name="Deposito_Destino" placeholder="Deposito de Destino" required>-->
                        </td>
                    </tr>

                    <tr>
                        <td>Tipo de Movimiento</td>
                        <td><input type="text" name="Tipo_de_Movimiento" placeholder="Tipo de Movimiento"></td>   
                    </tr>
                    
                    <tr>
                        <th>Nombre</th>
                        <th>Código</th>
                        <th>Cantidad</th>
                    </tr>
                    
                    <table>
    
                        <tr>
                            <td>
                                <select name="Nombre"  onclick="ListaCodigo(this.value)">

                                    <?php

                                        //se incluye el archivo donde se hace le proceso donde se muestra los nombres de los materiales existentes
                                        include "../../Materiales.php";

                                    ?>

                                </select>
                                <!--<input type="text" name="Nombre" placeholder="Nombre">-->
                            </td>
                            <td>
                                <select name="Código" id="Codigo">
                                
                                </select>  
                                <!--<input type="text" name="Código" placeholder="Código">-->
                            </td> 
                            <td><input type="number" name="Cantidad" placeholder="Cantidad"></td>                    
                        </tr>
                    
                    </table>
                    
                    <tr>
                        <td><input type="submit" value="Guardar"></td>
                        <td><input type="reset" value="Borrar"></td>
                    </tr>
                                
                </table>

            </form>

            <!--Botones que hay que revisar si es que se puede mejorar-->
            
            <!--Boton salir, envia a la pagina del menu-->
            <form action="../../index.php">
                <input type="submit" value="Salir" />
            </form>    
            
            <!--Boton que muestra lo que se cargo en la tabla--> 
            <form action="Listado.php">
                <input type="submit" value="Lista de Datos"/>
            </form>

        </div>

    </body>

</html>
