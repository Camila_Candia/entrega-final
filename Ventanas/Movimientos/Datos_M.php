<?php
    //PROCESO DE INSERTAR DATOS EN LA BASE DE DATOS 
    try
    {
        
        //conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
        //captura de datos que el usuario introduce en el formulario 
        $fecha= $_POST['Fecha'];
        $deposito_origen=$_POST['Deposito_Origen'];
        $deposito_destino=$_POST['Deposito_Destino'];
        $tipo_movimiento=$_POST['Tipo_de_Movimiento'];
        $hoy = date("Y:m:h:i:sa");

        //sentencia sql que prepara los datos par introducirlos en la tabla de movimientos
        $consulta_1= "INSERT INTO movimientos (fecha,deposito_origen,deposito_destino,tipo_de_movimiento,fecha_creacion) VALUES (?,?,?,?,?)";
        $sql_1=$conexion->prepare($consulta_1);

        //se insertan los datos en la tabla de movimientos
        $sql_1->bindParam(1,$fecha);
        $sql_1->bindParam(2,$deposito_origen);
        $sql_1->bindParam(3,$deposito_destino);
        $sql_1->bindParam(4,$tipo_movimiento);
        $sql_1->bindParam(5,$hoy);
        
        //ejecutamos la sentencia
        $sql_1->execute();

    }
    catch(PDOException $php_errormsg)
    {

        //si ocurre un error imprime lo siguiente
        echo "Ocurrio un error en la tabla de movimientos";

    }

    try
    {
        
        //conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
        //captura de datos que el usuario introduce en el formulario 
        $nombre=$_POST['Nombre'];
        $codigo=$_POST['Código'];
        $cantidad=$_POST['Cantidad'];

        //sentencia sql que prepara los datos para introducirlos en la tabla de movimientos
        $consulta_2= "INSERT INTO movimientos_det (nombre,codigo,cantidad) VALUES (?,?,?)";
        $sql_2=$conexion->prepare($consulta_2);

        //se insertan los datos en la tabla de movimientos
        $sql_2->bindParam(1,$nombre);
        $sql_2->bindParam(2,$codigo);
        $sql_2->bindParam(3,$cantidad);

        //ejecutamos la sentencia
        $sql_2->execute();
    
    }
    catch(PDOException $php_errormsg)
    {

        //si ocurre un error se imprime lo siguiente
        echo 'Ocurrio un error en la tabla de movimientos detalle';
    }
    
    //recarga la pagina despues de darle el boton guaradar 
    header("Location: Movimientos.php");

?>
