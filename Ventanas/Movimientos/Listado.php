<!DOCTYPE html>
<html lang="es">

    <head>
        
        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Listado de Datos de Movimientos</title>
    
    </head>

    <body>

        <!--Bloque de php en donde se hace el llamado del archivo dodnde se realiza el proceso de eliminar datos-->
        <?php

            //se hace el llamado al archivo de conexion de la base de datos 
            include "Conexion_BD.php";

            //se pregunta si hay datos relacionados con la id y entra en el ciclo
            if (isset($_GET['id']))
            {

                //hace el llamdo domde se hace el proceso de eliminar los datos
                include "Eliminar_M.php";
            
            }

        ?>

        <table>

            <tr>
                <!--Titulos de la lista-->
                <th>Fecha</th>
                <th>Deposito de Origen</th>
                <th>Deposito de Destino</th>
                <th>Tipo de Movimiento</th>
                <th>Nombre</th>
                <th>Código</th>
                <th>Cantidad</th>
            </tr>

            <tbody>

                <!--Bloque php en donde se hace el el llamdo a la conexion de la base de datos y el archivo donde se procesa los datos para mostrar los datos en pantalla-->
                <?php

                    //hace llamado al archivo donde se procesan los datos 
                    include "Conexion_BD.php";
                    include "Selecionar_M.php";
                
                ?>
        
            </tbody>      
        
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="Movimientos.php">
            <input type="submit" value="Volver al formulario" />
        </form>   
        <a href="Reporte_word.php" >Reporte en Word</a>
            <a href="Reporte_excel.php" >Reporte en Excel</a>
    
    </body>
</html>

