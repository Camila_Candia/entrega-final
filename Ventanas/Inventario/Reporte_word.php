<!DOCTYPE html>
<html lang="es">

    <head>

        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Listado de datos de Inventario Semanal Interno</title>
    
    </head>
    
    <body>

        

        <table>

            <tr>
                <!--Titulos de la lista-->
                <th>Fecha</th>
                <th>Recurso</th>
                <th>Nombre</th>
                <th>Código</th>
                <th>Cantidad</th>
                <th>Retazo</th>
            </tr>
         
            <tbody>

                <!--Bloque php en donde se hace el llamdo a la conexion de la base datos y al archivo donde se procesa los datos para mostralos en pantalla-->
                <?php

                    //hace llamado al archivo donde se procesan los datos 
                    include "Conexion_BD.php";
                    include "Reporte_w.php";
            
                ?>
        
             </tbody>   
            
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="Inventario.php">
            <input type="submit" value="Volver al formulario" />
        </form>    
    
    </body>

</html>
