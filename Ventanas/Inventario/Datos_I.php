
<?php

    //PROCESO DE DONDE SE INSERTAN LOS DATOS EN ÑLA BASE DE DATOS
    
    //conexion a la base de datos 
    $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
    //captura de datos que el usuario introduce en el formulario 
    $fecha=$_POST['Fecha'];
    $recurso=$_POST['Recurso'];
    $hoy = date("Y:m:d:h:i:sa");

    //sentencia sql que prepara los datos en la tabla de inventario semanal interno 
    $consulta_1= "INSERT INTO isi (fecha,recurso,fecha_creacion) VALUES (?,?,?) RETURNING id_isi";
    $sql_1=$conexion->prepare($consulta_1);

    //se insertan los datos en la tabla de inventario semanal interno
    $sql_1->bindParam(1,$fecha);
    $sql_1->bindParam(2,$recurso);
    $sql_1->bindParam(3,$hoy);

    //ejecutamos la sentencia
    $sql_1->execute();

    //los datos que se insertaron en la tabla se cola en un array asociativo
    $id=$sql_1->fetch(PDO::FETCH_ASSOC);
    
    //se pone dentro de una variable la id de la tabla de inventario semanal interno que esta presenete inventario semanla interno detalle
    $id_isi=$id["id_isi"];

    //si ocurre un error imprime el siguinte mensaje
    //echo "Ocurrio un error en la tabla de inventario semanam interno";
        
    //captura de datos que el usuario introduce en el formulario 
    $nombre=$_POST['Nombre'];
    $codigo= $_POST['Código'];
    $cantidad= $_POST['Cantidad'];
    $retazo=$_POST['Retazo'];

    //sentencia sql que prepara los datos en la tabla de inventario semanal interno detalle 
    $consulta_2= "INSERT INTO isi_det (id_isi,nombre,codigo,cantidad,retazo) VALUES (?,?,?,?,?)";
    $sql_2=$conexion->prepare($consulta_2);

    //se insertan los datos en la tabla de inventario semanal interno detalle 
    $sql_2->bindParam(1,$id_isi);    
    $sql_2->bindParam(2,$nombre);
    $sql_2->bindParam(3,$codigo);
    $sql_2->bindParam(4,$cantidad);
    $sql_2->bindParam(5,$retazo);

    //ejecutamos la consulta
    $sql_2->execute();        

    //si ocurre un error se imprime lo siguiente
    //echo 'Ocurrio un error en la tabla de iventario semanal interno detalle';

    //recarga la pagina despues de darle el boton guaradar 
    header("Location: Inventario.php");

?>
