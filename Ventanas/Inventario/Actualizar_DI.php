<?php

    //Actualizar los datos 
    //pregunta si los campos estan vacios si no lo estan entran al ciclo 
    if (isset($_POST['Fecha']) and isset($_POST['Recurso']))
    {

        //se capturan todos los datos que inserta el usuario 
        $id=$_POST['id'];// la id se incrementa sola
        $fecha=$_POST['Fecha'];
        $recurso=$_POST['Recurso'];

        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se hace la consulta sql para la tabla de inventario semanal interno
        $consulta_1=$conexion->prepare("UPDATE isi SET fecha=:fecha,recurso=:recurso WHERE id_isi=:id");

        //se introducen los nuevos datos
        $consulta_1->bindParam(":fecha",$fecha);
        $consulta_1->bindParam(":recurso",$recurso);
        $consulta_1->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_1->execute();

        //se capturan los datos que inserta el usuario
        $nombre=$_POST['Nombre'];
        $codigo=$_POST['Código'];
        $cantidad=$_POST['Cantidad'];
        $retazo=$_POST['Retazo'];

        //se hace la conexion de la base datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
        
        //se hace la consulta sql para la tabla de inventario semanal interno detalle 
        $consulta_2=$conexion->prepare("UPDATE isi_det SET nombre=:nombre, codigo=:codigo, cantidad=:cantidad, retazo=:retazo WHERE id_isi=:id");
        
        //se introducen los nuevos datos
        $consulta_2->bindParam(":nombre",$nombre);
        $consulta_2->bindParam(":codigo",$codigo);
        $consulta_2->bindParam(":cantidad", $cantidad);
        $consulta_2->bindParam(":retazo", $retazo);
        $consulta_2->bindParam(":id",$id);

        //ejecutamos la consulta
        $consulta_2->execute();

    }

    //Recuperar los datos
    //se pregunta si la id no esta vacia, si no lo esta entra en el ciclo y realiza la recupueración de los datos y los muestra en pantalla
    if(isset($_GET['id']))
    {

        $id=$_GET['id'];

        //se hace la conexion a la base de datos 
        $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

        //se seleccionan todos los datos de la tabla inventario semanal interno
        $consulta_1=$conexion->prepare("SELECT * FROM isi WHERE id_isi=:id");
        
        //se recuperan todos los datos de la tabla invnetario semanal interno
        $consulta_1->bindParam(":id",$id);

        //ejecutamos la sentencia
        $consulta_1->execute();

        //se selccionan todos los datos de la tabla inventario semanl interno detalle
        $consulta_2=$conexion->prepare("SELECT * FROM isi_det WHERE id_isi=:id2");

        //se recuperan todos los datos de la tabla inventario semanal interno detalle
        $consulta_2->bindParam(":id2",$id);

        //ejecutamos la snetencia
        $consulta_2->execute();

        //se pregunta si las tablas estan vacias si no la estan ingresan al ciclo
        if($consulta_1->rowCount()>=1 and $consulta_2->rowCount()>=1);
        { 
            //se ponen en un array asociativo para poder recorrerlas 
            $isi=$consulta_1->fetch();//en isi estan todos los datos de la tabla inventario semanal interno
            $isi_det=$consulta_2->fetch();// en isi_det estan todos los datos de la tabla inventaro semanal interno detalle

            //se muestra el formulario en pantalla para actualizacion 
            echo   '<form action="" method="POST">
                        <!--Se trae el id de la tabla inventario semanal interno pero no se muestra en pantalla-->
                        <input type="hidden" name="id" value="'.$isi['id_isi'].'">

                        <table>

                            <tr>    
                                <td>Fecha:</td>
                                <td><input type="date" name="Fecha" value="'.$isi['fecha'].'" placeholder="dd/mm/aa" required autofocus></td>
                            </tr>  
                            
                            <tr>
                                <td>Recurso:</td>
                                <td><input type="number" name="Recurso" value="'.$isi['recurso'].'" placeholder="Número de Recurso" required></td>
                            </tr>

                            
                            <input type="hidden" name="id" value="'.$isi_det['id_isi'].'">
                            <tr>
                                <th>Nombre</th>
                                <th>Código</th>
                                <th>Cantidad</th>
                                <th>Retazo</th>
                            </tr>
                            
                            <table>
                                
                                <tr>
                                    <td><input type="text" name="Nombre" value="'.$isi_det['nombre'].'" placeholder="Nombre"></td>
                                    <td><input type="text" name="Código" value="'.$isi_det['codigo'].'" placeholder="Código"></td> 
                                    <td><input type="number" name="Cantidad" value="'.$isi_det['cantidad'].'" placeholder="Cantidad"></td>
                                    <td><input type="number" name="Retazo" value="'.$isi_det['retazo'].'" placeholder="Retazo"></td>    
                                </tr>

                            </table>
                            
                            <tr>
                                <td><input type="submit" value="Guardar"></td>
                                <td><input type="reset" value="Borrar"></td>
                            </tr>
                            
                            
                        </table>
                
                    </form>';

        } 
    
    }
    else
    {

        echo "Error no se pudo procesar la solicitud";
    
    }
    
?>
