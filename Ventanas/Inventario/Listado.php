
<!DOCTYPE html>
<html lang="es">

    <head>

        <meta charset="UTF-8">
        <!--Titulo de la página-->
        <title>Listado de datos de Inventario Semanal Interno</title>
    
    </head>
    
    <body>

        <!--Bloque de php en donde se hace el llamado del archivo donde se realiza el proceso de eliminar datos-->
        <?php

            //hace llamado al archivo de conexion de la base datos 
            include "Conexion_BD.php";

            //se pregunta si hay datos relacionados con la id y entra en el ciclo
            if (isset($_GET['id']))
            {

                //hace el llamdo donde se hace el proceso de eliminar los datos 
                include "Eliminar_DI.php";

            }

        ?>

        <table>

            <tr>
                <!--Titulos de la lista-->
                <th>Fecha</th>
                <th>Recurso</th>
                <th>Nombre</th>
                <th>Código</th>
                <th>Cantidad</th>
                <th>Retazo</th>
            </tr>
         
            <tbody>

                <!--Bloque php en donde se hace el llamdo a la conexion de la base datos y al archivo donde se procesa los datos para mostralos en pantalla-->
                <?php

                    //hace llamado al archivo donde se procesan los datos 
                    include "Conexion_BD.php";
                    include "Selecionar_DI.php";
            
                ?>
        
             </tbody>   
            
        </table>
        
        <!--Boton que al darle click te devuelve al formulario-->
        <form action="Inventario.php">
            <input type="submit" value="Volver al formulario" />
        </form>    
        <a href="Reporte_word.php" >Reporte en Word</a>
            <a href="Reporte_excel.php" >Reporte en Excel</a>
    </body>

</html>
