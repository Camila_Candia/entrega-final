--Trigger y Funcion de la tabla Moviminento--

create or replace function auditoria_cmr() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_cmr" values(old.id_cmr,old.fecha,old.recurso,old.tecnologia,old.ot,old.tipo_de_tarea);
        return new;

        END
    $$
Language plpgsql

create trigger tr_cmr before update on cmr
    for each row
    execute procedure auditoria_cmr();
--------------------------------------------------------------------------------------------------------------------
create or replace function auditoria_cmr_det() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_cmr_det" values(old.id_cmr,old.nombre,old.codigo,old.cantidad);
        return new;

        END
    $$
Language plpgsql

create trigger tr_cmr_det before update on cmr_det
    for each row
    execute procedure auditoria_cmr_det();
----------------------------------------------------------------------------------------------------------------------
create or replace function auditoria_cbr() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_cbr" values(old.id_cbr,old.recurso);
        return new;

        END
    $$
Language plpgsql

create trigger tr_cbr before update on cbr
    for each row
    execute procedure auditoria_cbr();
---------------------------------------------------------------------------------------------------------------------------------------
create or replace function auditoria_cbr_det() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_cbr_det" values(old.id_cbr,old.inicio,old.final,old.fecha_de_entrega,old.fecha_de_devolucion,old.comentario);
        return new;

        END
    $$
Language plpgsql

create trigger tr_cbr_det before update on cbr_det
    for each row
    execute procedure auditoria_cbr_det();
------------------------------------------------------------------------------------------------------------------------------------------
create or replace function auditoria_isi() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_isi" values(old.id_isi,old.fecha,old.recurso);
        return new;

        END
    $$
Language plpgsql

create trigger tr_isi before update on isi
    for each row
    execute procedure auditoria_isi();
---------------------------------------------------------------------------------------------------------------------
create or replace function auditoria_isi_det() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_isi_det" values(old.id_isi,old.nombre,old.codigo,old.cantidad,old.retazo);
        return new;

        END
    $$
Language plpgsql

create trigger tr_isi_det before update on isi_det 
    for each row
    execute procedure auditoria_isi_det();
--------------------------------------------------------------------------------------------------------------------------
create or replace function auditoria_asignar_equipos() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_asignar_equipos" values(old.id_ae,old.fecha,old.recurso);
        return new;

        END
    $$
Language plpgsql

create trigger tr_asiganar_equipos before update on asignar_equipos
    for each row
    execute procedure auditoria_asignar_equipos();
------------------------------------------------------------------------------------------------------------
create or replace function auditoria_asignar_equipos_det() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_asignar_equipos_det" values(old.id_ae,old.numero_de_serial,old.numero_de_tarjeta,old.u_a);
        return new;

        END
    $$
Language plpgsql

create trigger tr_asiganar_equipos_det before update on asignar_equipos_det
    for each row
    execute procedure auditoria_asignar_equipos_det();
---------------------------------------------------------------------------------------------------------------------------
create or replace function auditoria_movimientos() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_movimientos" values(old.id_movimiento,old.fecha,old.deposito_origen,old.deposito_destino,old.tipo_de_movimiento);
        return new;

        END
    $$
Language plpgsql

create trigger tr_movimientos before update on movimientos
    for each row
    execute procedure auditoria_movimientos();
------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function auditoria_movimientos_det() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_movimientos_det" values(old.id_movimiento,old.nombre,old.codigo,old.cantidad);
        return new;

        END
    $$
Language plpgsql

create trigger tr_movimientos_det before update on movimientos_det
    for each row
    execute procedure auditoria_movimientos_det();	
--------------------------------------------------------------------------------------------------------------------	
create or replace function auditoria_smr() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_smr" values(old.id_smr,old.fecha,old.recurso,old.tipo_de_recurso);
        return new;

        END
    $$
Language plpgsql

create trigger tr_smr before update on smr
    for each row
    execute procedure auditoria_smr();
-------------------------------------------------------------------------------------------------------------------------
create or replace function auditoria_smr_det() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_smr_det" values(old.id_smr,old.nombre,old.codigo,old.cantidad);
        return new;

        END
    $$
Language plpgsql

create trigger tr_smr_det before update on smr_det
    for each row
    execute procedure auditoria_smr_det();
---------------------------------------------------------------------------------------------------------------------
create or replace function auditoria_equipos_retirados() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_equipos_retirados" values(old.id_er,old.fecha,old.recurso,old.partner);
        return new;

        END
    $$
Language plpgsql

create trigger tr_equipos_retirados before update on equipos_retirados
    for each row
    execute procedure auditoria_equipos_retirados();
----------------------------------------------------------------------------------------------------------------------
create or replace function auditoria_equipos_retirados_det() returns Trigger 
    AS
    $$
        BEGIN 

        INSERT INTO "aud_equipos_retirados_det" values(old.id_er,old.modelo,old.numero_de_tarjeta,old.numero_de_serial,old.u_a,old.tipo,old.tipo_de_tarea,old.falla,old.ot,old.ab);
        return new;

        END
    $$
Language plpgsql

create trigger tr_equipos_retirados_det before update on equipos_retirados_det
    for each row
    execute procedure auditoria_equipos_retirados_det();
