/*==============================================================*/
/* Table: AUD_ASIGNAR_EQUIPOS                                       */
/*==============================================================*/
create table AUD_ASIGNAR_EQUIPOS 
(
   ID_AE                SERIAL               not null,
   FECHA                DATE                 null,
   RECURSO              NUMERIC(3)           null,
   constraint PK_ASIGNAR_EQUIPOS primary key (ID_AE)
);

/*==============================================================*/
/* Table: AUD_ASIGNAR_EQUIPOS_DET                                   */
/*==============================================================*/
create table AUD_ASIGNAR_EQUIPOS_DET 
(
   ID_AE                SERIAL               not null,
   NUMERO_DE_SERIAL     VARCHAR(30)          null,
   NUMERO_DE_TARJETA    VARCHAR(30)          null,
   U_A                  VARCHAR(30)          null,
   constraint PK_ASIGNAR_EQUIPOS_DET primary key (ID_AE_DET)
);

/*==============================================================*/
/* Table: AUD_CBR                                                   */
/*==============================================================*/
create table AUD_CBR 
(
   ID_CBR               SERIAL               not null,
   RECURSO              NUMERIC(3)           null,
   constraint PK_CBR primary key (ID_CBR)
);

/*==============================================================*/
/* Table: AUD_CBR_DET                                               */
/*==============================================================*/
create table AUD_CBR_DET 
(
   ID_CBR               SERIAL               not null,
   INICIO               NUMERIC(20)          null,
   FINAL                NUMERIC(20)          null,
   FECHA_DE_ENTREGA     DATE                 null,
   FECHA_DE_DEVOLUCION  DATE                 null,
   COMENTARIO           VARCHAR(30)          null,
   constraint PK_CBR_DET primary key (ID_CBR_DET)
);

/*==============================================================*/
/* Table: AUD_CMR                                                   */
/*==============================================================*/
create table AUD_CMR 
(
   ID_CMR               SERIAL               not null,
   FECHA                DATE                 null,
   RECURSO              NUMERIC(3)           null,
   TECNOLOGIA           VARCHAR(30)          null,
   OT                   NUMERIC(20)          null,
   TIPO_DE_TAREA        VARCHAR(30)          null,
   constraint PK_CMR primary key (ID_CMR)
);

/*==============================================================*/
/* Table: AUD_CMR_DET                                               */
/*==============================================================*/
create table AUD_CMR_DET 
(
   ID_CMR               SERIAL               not null,
   NOMBRE               VARCHAR(60)          null,
   CODIGO               VARCHAR(40)          null,
   CANTIDAD             NUMERIC(20)          null,
   constraint PK_CMR_DET primary key (ID_CMR_DET)
);

/*==============================================================*/
/* Table: AUD_EQUIPOS_RETIRADOS                                     */
/*==============================================================*/
create table AUD_EQUIPOS_RETIRADOS 
(
   ID_ER                SERIAL               not null,
   FECHA                DATE                 null,
   RECURSO              NUMERIC(3)           null,
   PATNER               VARCHAR(30)          null,
   constraint PK_EQUIPOS_RETIRADOS primary key (ID_ER)
);

/*==============================================================*/
/* Table: AUD_EQUIPOS_RETIRADOS_DET                                 */
/*==============================================================*/
create table AUD_EQUIPOS_RETIRADOS_DET 
(
   ID_ER                SERIAL               not null,
   MODELO               VARCHAR(30)          null,
   NUMERO_DE_SERIAL     VARCHAR(30)          null,
   NUMERO_DE_TARJETA    VARCHAR(30)          null,
   U_A                  VARCHAR(30)          null,
   TIPO                 VARCHAR(30)          null,
   TIPO_DE_TAREA        VARCHAR(30)          null,
   FALLA                VARCHAR(30)          null,
   OT                   NUMERIC(30)          null,
   AB                   NUMERIC(20)          null,
   constraint PK_EQUIPOS_RETIRADOS_DET primary key (ID_ER_DET)
);

/*==============================================================*/
/* Table: AUD_ISI                                                   */
/*==============================================================*/
create table AUD_ISI 
(
   ID_ISI               SERIAL               not null,
   FECHA                DATE                 null,
   RECURSO              NUMERIC(3)           null,
   FECHA_CREACION       VARCHAR(30)          not null,
   constraint PK_ISI primary key (ID_ISI)
);

/*==============================================================*/
/* Table: AUD_ISI_DET                                               */
/*==============================================================*/
create table AUD_ISI_DET 
(
   ID_ISI               SERIAL               not null,
   NOMBRE               VARCHAR(30)          null,
   CODIGO               VARCHAR(30)          null,
   CANTIDAD             NUMERIC(20)          null,
   RETAZO               NUMERIC(20)          null,
   constraint PK_ISI_DET primary key (ID_ISI_DET)
);

/*==============================================================*/
/* Table: AUD_MOVIMIENTOS                                           */
/*==============================================================*/
create table AUD_MOVIMIENTOS 
(
   ID_MOVIMIENTO        SERIAL               not null,
   FECHA                DATE                 null,
   DEPOSITO_ORIGEN      VARCHAR(30)          null,
   DEPOSITO_DESTINO     VARCHAR(30)          null,
   TIPO_DE_MOVIMIENTO   VARCHAR(30)          null,
   constraint PK_MOVIMIENTOS primary key (ID_MOVIMIENTO)
);

/*==============================================================*/
/* Table: AUD_MOVIMIENTO_DET                                        */
/*==============================================================*/
create table AUD_MOVIMIENTOS_DET 
(
   ID_MOVIMIENTO_DET    SERIAL               not null,
   ID_MOVIMIENTO        SERIAL               not null,
   NOMBRE               VARCHAR(60)          null,
   CODIGO               VARCHAR(40)          null,
   CANTIDAD             NUMERIC(20)          null,
   constraint PK_MOVIMIENTO_DET primary key (ID_MOVIMIENTO)
);

/*==============================================================*/
/* Table: AUD_SMR                                                   */
/*==============================================================*/
create table AUD_SMR 
(
   ID_SMR               SERIAL               not null,
   FECHA                DATE                 null,
   RECURSO              NUMERIC(3)           null,
   TIPO_DE_RECURSO      VARCHAR(30)          null,
   constraint PK_SMR primary key (ID_SMR)
);

/*==============================================================*/
/* Table: AUD_SMR_DET                                               */
/*==============================================================*/
create table AUD_SMR_DET 
(
   ID_SMR               SERIAL               not null,
   NOMBRE               VARCHAR(60)          null,
   CODIGO               VARCHAR(40)          null,
   CANTIDAD             NUMERIC(20)          null,
   constraint PK_SMR_DET primary key (ID_SMR_DET)
);



-----------------------------------------------------------------

/*==============================================================*/
/* Table: ASIGNAR_EQUIPOS                                       */
/*==============================================================*/
create table ASIGNAR_EQUIPOS 
(
   ID_AE                SERIAL               not null,
   FECHA                DATE                 null,
   RECURSO              NUMERIC(3)           null,
   FECHA_CREACION       VARCHAR(30)          not null,
   constraint PK_ASIGNAR_EQUIPOS primary key (ID_AE)
);

/*==============================================================*/
/* Table: ASIGNAR_EQUIPOS_DET                                   */
/*==============================================================*/
create table ASIGNAR_EQUIPOS_DET 
(
   ID_AE_DET            SERIAL               not null,
   ID_AE                SERIAL               not null,
   NUMERO_DE_SERIAL     VARCHAR(30)          null,
   NUMERO_DE_TARJETA    VARCHAR(30)          null,
   U_A                  VARCHAR(30)          null,
   constraint PK_ASIGNAR_EQUIPOS_DET primary key (ID_AE_DET)
);

/*==============================================================*/
/* Table: CBR                                                   */
/*==============================================================*/
create table CBR 
(
   ID_CBR               SERIAL               not null,
   RECURSO              NUMERIC(3)           null,
   FECHA_CREACION       VARCHAR(30)          not null,
   constraint PK_CBR primary key (ID_CBR)
);

/*==============================================================*/
/* Table: CBR_DET                                               */
/*==============================================================*/
create table CBR_DET 
(
   ID_CBR_DET           SERIAL               not null,
   ID_CBR               SERIAL               not null,
   INICIO               NUMERIC(20)          null,
   FINAL                NUMERIC(20)          null,
   FECHA_DE_ENTREGA     DATE                 null,
   FECHA_DE_DEVOLUCION  DATE                 null,
   COMENTARIO           VARCHAR(30)          null,
   constraint PK_CBR_DET primary key (ID_CBR_DET)
);

/*==============================================================*/
/* Table: CMR                                                   */
/*==============================================================*/
create table CMR 
(
   ID_CMR               SERIAL               not null,
   FECHA                DATE                 null,
   RECURSO              NUMERIC(3)           null,
   TECNOLOGIA           VARCHAR(30)          null,
   OT                   NUMERIC(20)          null,
   TIPO_DE_TAREA        VARCHAR(30)          null,
   FECHA_CREACION       VARCHAR(30)          not null,
   constraint PK_CMR primary key (ID_CMR)
);

/*==============================================================*/
/* Table: CMR_DET                                               */
/*==============================================================*/
create table CMR_DET 
(
   ID_CMR_DET           SERIAL               not null,
   ID_CMR               SERIAL               not null,
   NOMBRE               VARCHAR(60)          null,
   CODIGO               VARCHAR(40)          null,
   CANTIDAD             NUMERIC(20)          null,
   constraint PK_CMR_DET primary key (ID_CMR_DET)
);

/*==============================================================*/
/* Table: DEPOSITOS                                             */
/*==============================================================*/
create table DEPOSITOS 
(
   ID_DEPOSITO          SERIAL               not null,
   NOMBRE               VARCHAR(30)          null,
   FECHA_CREACION       VARCHAR(30)          not null,
   constraint PK_DEPOSITOS primary key (ID_DEPOSITO)
);

/*==============================================================*/
/* Table: EQUIPOS_RETIRADOS                                     */
/*==============================================================*/
create table EQUIPOS_RETIRADOS 
(
   ID_ER                SERIAL               not null,
   FECHA                DATE                 null,
   RECURSO              NUMERIC(3)           null,
   PATNER               VARCHAR(30)          null,
   FECHA_CREACION       VARCHAR(30)          null,
   constraint PK_EQUIPOS_RETIRADOS primary key (ID_ER)
);

/*==============================================================*/
/* Table: EQUIPOS_RETIRADOS_DET                                 */
/*==============================================================*/
create table EQUIPOS_RETIRADOS_DET 
(
   ID_ER_DET            SERIAL               not null,
   ID_ER                SERIAL               not null,
   MODELO               VARCHAR(30)          null,
   NUMERO_DE_SERIAL     VARCHAR(30)          null,
   NUMERO_DE_TARJETA    VARCHAR(30)          null,
   U_A                  VARCHAR(30)          null,
   TIPO                 VARCHAR(30)          null,
   TIPO_DE_TAREA        VARCHAR(30)          null,
   FALLA                VARCHAR(30)          null,
   OT                   NUMERIC(30)          null,
   AB                   NUMERIC(20)          null,
   constraint PK_EQUIPOS_RETIRADOS_DET primary key (ID_ER_DET)
);

/*==============================================================*/
/* Table: ISI                                                   */
/*==============================================================*/
create table ISI 
(
   ID_ISI               SERIAL               not null,
   FECHA                DATE                 null,
   RECURSO              NUMERIC(3)           null,
   FECHA_CREACION       VARCHAR(30)          not null,
   constraint PK_ISI primary key (ID_ISI)
);

/*==============================================================*/
/* Table: ISI_DET                                               */
/*==============================================================*/
create table ISI_DET 
(
   ID_ISI_DET           SERIAL               not null,
   ID_ISI               SERIAL               not null,
   NOMBRE               VARCHAR(30)          null,
   CODIGO               VARCHAR(30)          null,
   CANTIDAD             NUMERIC(20)          null,
   RETAZO               NUMERIC(20)          null,
   constraint PK_ISI_DET primary key (ID_ISI_DET)
);

/*==============================================================*/
/* Table: MATERIALES                                            */
/*==============================================================*/
create table MATERIALES 
(
   ID_MATERIALES        SERIAL               not null,
   NOMBRE               VARCHAR(60)          null,
   CODIGO               VARCHAR(40)          null,
   FECHA_CREACION       VARCHAR(30)          not null,
   constraint PK_MATERIALES primary key (ID_MATERIALES)
);

/*==============================================================*/
/* Table: MATERIALES_DEPOSITO                                   */
/*==============================================================*/
create table MATERIALES_DEPOSITO 
(
   ID_MD                SERIAL               not null,
   ID_DEPOSITO          SERIAL               not null,
   ID_MATERIAL          SERIAL               not null,
   CANTIDAD             NUMERIC(20)          null,
   STOCK_MIN            NUMERIC(20)          null,
   STOCK_MAX            NUMERIC(20)          null,
   FECHA_CREACION       VARCHAR(30)          not null,
   constraint PK_MATERIALES_DEPOSITO primary key (ID_MD)
);

/*==============================================================*/
/* Table: MOVIMIENTOS                                           */
/*==============================================================*/
create table MOVIMIENTOS 
(
   ID_MOVIMIENTO        SERIAL               not null,
   FECHA                DATE                 null,
   DEPOSITO_ORIGEN      VARCHAR(30)          null,
   DEPOSITO_DESTINO     VARCHAR(30)          null,
   TIPO_DE_MOVIMIENTO   VARCHAR(30)          null,
   FECHA_CREACION       VARCHAR(30)          not null,
   constraint PK_MOVIMIENTOS primary key (ID_MOVIMIENTO)
);

/*==============================================================*/
/* Table: MOVIMIENTO_DET                                        */
/*==============================================================*/
create table MOVIMIENTOS_DET 
(
   ID_MOVIMIENTO_DET    SERIAL               not null,
   ID_MOVIMIENTO        SERIAL               not null,
   NOMBRE               VARCHAR(60)          null,
   CODIGO               VARCHAR(40)          null,
   CANTIDAD             NUMERIC(20)          null,
   constraint PK_MOVIMIENTO_DET primary key (ID_MOVIMIENTO)
);

/*==============================================================*/
/* Table: SMR                                                   */
/*==============================================================*/
create table SMR 
(
   ID_SMR               SERIAL               not null,
   FECHA                DATE                 null,
   RECURSO              NUMERIC(3)           null,
   TIPO_DE_RECURSO      VARCHAR(30)          null,
   FECHA_CREACION       VARCHAR(30)          not null, 
   constraint PK_SMR primary key (ID_SMR)
);

/*==============================================================*/
/* Table: SMR_DET                                               */
/*==============================================================*/
create table SMR_DET 
(
   ID_SMR_DET           SERIAL               not null,
   ID_SMR               SERIAL               not null,
   NOMBRE               VARCHAR(60)          null,
   CODIGO               VARCHAR(40)          null,
   CANTIDAD             NUMERIC(20)          null,
   constraint PK_SMR_DET primary key (ID_SMR_DET)
);

/*==============================================================*/
/* Table: USUARIOS                                              */
/*==============================================================*/
create table USUARIOS 
(
   ID_USUARIO           SERIAL               not null,
   NOMBRE               VARCHAR(30)          null,
   APELLIDO	            VARCHAR(30)		     null,
   USUARIO	            VARCHAR(30)		     null,
   CONTRASEÑA           VARCHAR(100)         null,
   FECHA_CREACION       VARCHAR(30)          not null,
   constraint PK_USUARIOS primary key (ID_USUARIO)
);

alter table ASIGNAR_EQUIPOS_DET
   add constraint FK_ASIGNAR__REFERENCE_ASIGNAR_ foreign key (ID_AE)
      references ASIGNAR_EQUIPOS (ID_AE)
      on delete cascade on update cascade;


alter table CBR_DET
   add constraint FK_CBR_DET_REFERENCE_CBM foreign key (ID_CBR)
      references CBR (ID_CBR)
      on delete cascade on update cascade;

alter table CMR_DET
   add constraint FK_CMR_DET_REFERENCE_CMR foreign key (ID_CMR)
      references CMR (ID_CMR)
      on delete cascade on update cascade;

alter table EQUIPOS_RETIRADOS_DET
   add constraint FK_EQUIPOS__REFERENCE_EQUIPOS_ foreign key (ID_ER)
      references EQUIPOS_RETIRADOS (ID_ER)
      on delete cascade on update cascade;

alter table ISI_DET
   add constraint FK_ISI_DET_REFERENCE_ISI foreign key (ID_ISI)
      references ISI (ID_ISI)
      on delete cascade on update cascade;

alter table MATERIALES_DEPOSITO
   add constraint FK_MATERIAL_REFERENCE_DEPOSITO foreign key (ID_DEPOSITO)
      references DEPOSITOS (ID_DEPOSITO)
      on delete cascade on update cascade;

alter table MATERIALES_DEPOSITO
   add constraint FK_MATERIAL_REFERENCE_MATERIAL foreign key (ID_MATERIAL)
      references MATERIALES (ID_MATERIALES)
      on delete cascade on update cascade;

alter table MOVIMIENTOS_DET
   add constraint FK_MOVIMIEN_REFERENCE_MOVIMIEN foreign key (ID_MOVIMIENTO)
      references MOVIMIENTOS (ID_MOVIMIENTO)
      on delete cascade on update cascade;

alter table SMR_DET
   add constraint FK_SMR_DET_REFERENCE_SMR foreign key (ID_SMR)
      references SMR (ID_SMR)
      on delete cascade on update cascade;

